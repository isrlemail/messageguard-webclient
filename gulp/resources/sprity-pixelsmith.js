/**
 * Created by Scott on 9/11/2015.
 *
 * Pixelsmith-based engine for sprity.
 */

'use strict';

var Pixelsmith = require('pixelsmith'),
    pixelsmith = new Pixelsmith();

module.exports = {
  create: function (tiles, opt) {
    // Grab the variables.
    var format = opt.options && opt.options.format ? opt.options.format : 'png';
    var images = [];
    var positions = [];
    for (var i = 0; i < tiles.length; i++) {
      var tile = tiles[i];
      images[i] = tile.path;
      positions[i] = {x: tile.x + tile.offset, y: tile.y + tile.offset}
    }

    // Figure out the background color.
    var background;
    if (format === 'jpg') {
      background = [opt.bgColor[0], opt.bgColor[1], opt.bgColor[2], 100];
    } else {
      background = [opt.bgColor[0], opt.bgColor[1], opt.bgColor[2], opt.bgColor[3]];
    }

    // Create the image.
    return new Promise(function (resolve, reject) {
      // Load the tiles.
      pixelsmith.createImages(images, function handleImages(err, imgs) {
        if (err) {
          reject(err);
          return;
        }

        // Create the canvas with the desired background.
        var canvas = pixelsmith.createCanvas(opt.width, opt.height);
        for (i = 0; i < opt.width; i++) {
          for (var j = 0; j < opt.height; j++) {
            for (var k = 0; k < 4; k++) {
              canvas.ndarray.set(i, j, k, background[k]);
            }
          }
        }

        // Add each sprite.
        imgs.forEach(function addImages(img, i) {
          var position = positions[i];
          canvas.addImage(img, position.x, position.y);
        }, canvas);

        // Export canvas to am image.
        var resultStream = canvas['export']({format: format});
        var data = [];
        resultStream.on('data', function (chunk) {
          data.push(chunk);
        });
        resultStream.on('end', function () {
          resolve({
                    type:     format,
                    mimeType: 'image/' + format,
                    contents: Buffer.concat(data),
                    width:    opt.width,
                    height:   opt.height
                  });
        });
      });
    });
  },
  scale:  function (base, opt) {
    throw "Image scaling not supported in sprity-pixelsmith!";
  }
};