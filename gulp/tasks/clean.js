/**
 * Clean tasks.
 */
var gulp = require('gulp');

// Clean up any temporary files.
gulp.task('clean', ['js:clean', 'sass:clean', 'extension:clean', 'server:clean', 'sprite:clean', 'test:clean']);
