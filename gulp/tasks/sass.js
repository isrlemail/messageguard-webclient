/**
 * Front end tasks.
 */
"use strict";

var autoprefixer = require('gulp-autoprefixer'),
    config       = require('../config'),
    del          = require('del'),
    gulp         = require('gulp'),
    gwatch       = require('../resources/gulp-watch'),
    sass         = require('gulp-sass'),
    sourcemaps   = require('gulp-sourcemaps');


// Build all of the bundles.
gulp.task('sass', ['sass:files']);

// Clean up the build directory.
gulp.task('sass:clean', ['sprite:clean'], function () {
  return del(config.sass.buildPath);
});

/**
 * Build the Sass files.
 * @param partial Whether to only operated on changed files or on all files.
 */
var buildSass = function (partial) {
  return buildSassFiles(partial ?
                        gwatch.src(config.sass.srcFiles, null, ['sass:files:nodeps']) :
                        gulp.src(config.sass.srcFiles));
};

/**
 * Build Sass from a provided source stream.
 * @param src Source stream to build from.
 */
var buildSassFiles = function (src) {
  return src.pipe(sourcemaps.init())
      .pipe(sass({
                   outputStyle:  global.flags.debug ? 'nested' : 'compact',
                   includePaths: config.sass.includePaths
                 }).on('error', sass.logError))
      .pipe(autoprefixer())
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest(config.sass.buildPath + 'css'));
};

// Find out if we are already watching everything.
var alreadyWatched;

// Compile all of our sass files.
gulp.task('sass:files', ['sass:font-awesome', 'sass:bootstrap', 'sprite'], function () {
  // Watch the entire sass directory. We don't have something like watchify so we just rebuild the entire directory.
  if (!alreadyWatched && global.flags.watch) {
    alreadyWatched = true;
    gulp.watch(config.sass.srcPartialFiles, ['sass:files:full']);
  }

  return buildSass(true);
});

// Compile all of our sass files.
gulp.task('sass:files:nodeps', function () {
  return buildSass(true);
});

// Compile all of our sass files.
gulp.task('sass:files:full', function () {
  return buildSass(false);
});

// Task that only builds the front end.
gwatch.task('sass:frontend', ['sprite:frontend'], function () {
  return buildSassFiles(gwatch.src(config.sass.srcPath + 'frontend.scss', null, ['sass:frontend:nodeps']));
});

// Task that only builds the front end.
gulp.task('sass:messageGuard', function () {
  return buildSassFiles(gwatch.src(config.sass.srcPath + 'messageguard.scss', null, ['sass:messageGuard']));
});

// Prepare font-awesome fonts for use with sass.
gulp.task('sass:font-awesome', function () {
  return gulp.src([
                    config.bower.srcPath + 'fontawesome/fonts/**.*',
                    config.bower.srcPath + 'bootstrap-sass/assets/fonts/bootstrap/**.*'
                  ]).pipe(gulp.dest(config.sass.buildPath + 'fonts'));
});

gulp.task('sass:bootstrap', function () {
  return gulp.src(config.bower.srcPath + 'bootstrap-sass/assets/fonts/bootstrap/**.*')
      .pipe(gulp.dest(config.sass.buildPath + 'css/fonts/bootstrap'));
});
