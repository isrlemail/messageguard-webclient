/**
 * Configuration properties for the gulp tasks.
 */

var merge      = require('merge'),
    path       = require('path'),
    userConfig = require('./user-config'),
    util       = require('util');

/**
 * Configuration data for the gulp tasks.
 */
module.exports = merge.recursive(true, {

  /**
   * Configuration regarding bower.
   */
  bower: {
    srcPath: './bower_components/'
  },

  /**
   * Configuration for html files.
   */
  html: {
    srcPath:  './src/html/',
    srcFiles: './src/html/**/*.html'
  },

  /**
   * Configuration for the javascript builds.
   */
  js: {
    srcPath:  './src/js/',
    srcFiles: './src/js/**/*.{js,json}',

    buildPath:  './build/staging/js/',
    buildFiles: ['./build/staging/js/**/*.{js,json,map}', '!./build/staging/js/bookmarklet.{js,js.map}',
                 '!./build/staging/js/extension.{js,js.map}'],

    buildBookmarkletFiles: './build/staging/js/bookmarklet.{js,js.map}',
    buildExtensionFiles:   './build/staging/js/extension.{js,js.map}',

    lintPreferences: {
      eqeqeq:       true,
      forin:        true,
      freeze:       true,
      globalstrict: false,
      latedef:      true,
      noarg:        true,
      nonew:        true,
      undef:        true,
      unused:       true,

      browser:    true,
      browserify: true,
      devel:      true,
      worker:     true
    }
  },

  /**
   * Path to the resources. These are files included as is in the deployment.
   */
  resources: {
    srcPath:  './src/resources/',
    srcFiles: './src/resources/**/*'
  },

  /**
   * Configuration for SaSS and sprite.
   */
  sass: {
    srcPath:         './src/sass/',
    srcFiles:        './src/sass/**/[!_]*.scss',
    srcPartialFiles: ['./src/sass/**/_*.scss', './build/staging/sprite/**/_.scss'],

    buildPath:  './build/staging/sass/',
    buildFiles: './build/staging/sass/**/*',

    // Paths to bower modules.
    includePaths: [
      './bower_components/bootstrap-sass/assets/stylesheets/',
      './bower_components/bourbon/app/assets/stylesheets/',
      './bower_components/fontawesome/scss/',
      './bower_components/semantic/dist',
      './build/staging/sprite/'
    ]
  },

  /**
   * Build information for sprites.
   */
  sprite: {
    srcPath:          './src/sprite/',
    srcFiles:         './src/sprite/**/*.{png,jpg,jpeg}',
    frontendSrcFiles: './src/sprite/frontend/**/*.{png,jpg,jpeg}',

    buildPath:  './build/staging/sprite/',
    buildFiles: './build/staging/sprite/*.{png,jpg,jpeg}',

    sprityOptions: {
      src: './src/sprite/**/*.{png,jpg,jpeg}',
      out: './build/staging/sprite/',

      // Using an absolute path, since paths are resolved relative to the
      // loader script inside of sprity.
      // If we eventually make this engine support scaling, we could publish it as
      // its own npm module, and remove this absolute path.
      engine: path.resolve(__dirname, './resources/sprity-pixelsmith'),
      format: 'png',
      margin: 0,

      cssPath: '',
      name:    'sprite',
      prefix:  'sprite-background',

      template: './gulp/resources/sprite.hbs',

      style: '_sprite.scss'
    }
  },

  /**
   * Configuration for the extension.
   */
  extension: {
    stagingPath:         './build/staging/extension/',
    stagingResourcePath: './build/staging/extension/src/common/res/',
    stagingFiles:        './build/staging/extension/**/*',
    buildPath:           './build/output/extension/',

    templateFiles: './build/resources/extension/build_template/**/*',

    buildTargets: (function () {
      var kangoExec = 'python build/resources/extension/kango/kango.py';
      var stagingDir = 'build/staging/extension';
      var outputDir = 'build/output/extension';
      var platforms = ['chrome', 'safari', 'firefox'];

      var baseCommand = util.format('%s build %s --output-directory=%s', kangoExec, stagingDir, outputDir);
      var targets = {all: baseCommand};

      platforms.forEach(function (platform) {
        targets[platform] = util.format('%s --target=%s', baseCommand, platform);
      });
      return targets;
    })(),

    defaultPlatform: 'chrome'
  },

  /**
   * Configuration for the server.
   */
  server: {
    bookmarkletFile: './build/resources/server/bookmarklet.js',
    siteFiles:       './build/resources/server/site/**/*',

    buildPath:  './build/output/server/',
    buildFiles: './build/output/server/**/*',

    sftpOptions: null, // Defined in user config.
    baseUrl:     null  // Defined in user config.
  },

  /**
   * Replacements run on bundled source code.
   */
  replacements: {
    // Term object keys are replaced with their associated values.
    terms:  {
      MessageGuardId: 'MessageGuardScript'
    },
    // labels.server and labels.isExtension are names that will be replaced
    // by the server base URL and a boolean flag indicating whether the
    // code is running as an extension, respectively.
    labels: {
      server:      'Server',
      isExtension: 'IsExtension'
    }
  },

  /**
   * Configuration for the tests.
   */
  test: {
    performancePath:  './test-src/performance/',
    performanceFiles: ['./test-src/performance/**/*', '!./test-src/performance/test.js'],

    buildPath:  './build/test/',
    buildFiles: './build/test/**/*',

    sftpOptions: null, // Defined in user config.
    baseUrl:     null  // Defined in user config.
  }
}, userConfig);
