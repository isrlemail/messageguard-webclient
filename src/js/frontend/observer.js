/**
 * Observer that watches a single element. Allows multiple watches. For performance reasons we only use a single
 *    {@link MutationObserver}.
 * @module
 * @requires common/selectors
 */
"use strict";

require('../common/selectors');

/**
 * Create an observer for the given target.
 * @param {!Element} target Element to observer.
 * @constructor
 * @alias module:frontend/observer
 */
var Observer = function (target) {

  /**
   * The target element we are watching.
   * @type {Element}
   * @private
   */
  this._target = target;

  /**
   * The underlying MutationObserver.
   * @type {MutationObserver}
   * @private
   */
  this._observer = new MutationObserver(function (records) { this._onMutation(records); }.bind(this));

  /**
   * The _matchers that we are testing against.
   * @type {Object[]}
   * @prop {!MutationObserverInit} options MutationObserverInit options. Can also set a custom 'added', 'removed', or
   *    'modified' tag to true if you are not interested in getting both events with 'childList'. Also allows you to set
   *    a 'elementOnly' or 'textOnly' to specify which changes to the childList are interesting.
   * @prop {string} selector CSS selector to filter events with.
   * @prop {!module:frontend/observer~ElementMutated} callback Callback to execute when the associated options and
   *     selector are matched in an event.
   * @private
   */
  this._matchers = [];

  /**
   * Whether or not the MutationObserver is live.
   * @type {boolean}
   * @private
   */
  this._running = false;
};

/**
 * Set of possible mutation types.
 * @readonly
 * @enum {Number}
 */
Observer.MutationType = {
  /** Attribute changed. */
  Attributes:     1,
  /** CharacterData node was changed. */
  CharacterData:  2,
  /** Element was added. */
  ElementAdded:   3,
  /** Element was removed. */
  ElementRemoved: 4,
  /** Element was movied (i.e., removed and added to new parent). */
  ElementMoved:   5,
  /** Text node was added. */
  TextAdded:      6,
  /** Text node was removed. */
  TextRemoved:    7,
  /** Text node was modified. */
  TextModified:   8
};

/**
 * This callback is executed when an event matching the appropriate variables is fired.
 * @callback module:frontend/observer~ElementMutated
 * @param {!Element} element Element that has been mutated.
 * @param {!MutationRecord} record record MutationRecord associated with this mutation.
 * @param {!Number} type Type of the mutation.
 */

/**
 * Fired when there has been a mutation. We further refine these to see if there are actually events we are interested
 * in.
 * @param {!MutationRecord[]} records Mutations records.
 * @private
 */
Observer.prototype._onMutation = function (records) {
  // Don't fire mutations based on our events.
  this.stop();

  // Test the matchers against each record.
  for (var i = 0, len = records.length; i < len; i++) {
    var record = records[i],
        type   = record.type,
        target = record.target;
    if (target instanceof window.CharacterData) target = target.parentNode;

    for (var j = 0, len2 = this._matchers.length; j < len2; j++) {
      var matcher      = this._matchers[j],
          options      = matcher.options,
          selector     = matcher.selector,
          childList    = options.childList || options.added || options.removed || options.modified,
          subtree      = options.subtree,
          textOnly     = options.textOnly,
          elementsOnly = options.elementsOnly,
          callback     = matcher.callback;

      // Ensure that the event type is appropriate for the matcher.
      if (!(options[type] || (type === 'childList' && childList))) continue;
      if (!subtree && this._target !== target) continue;

      // React differently based on the type.
      switch (type) {
        case 'attributes':
          // Ignore if we are filter attribute names.
          if (options.attributeFilter && options.attributeFilter.length &&
              options.attributeFilter.indexOf(record.attributeName) === -1) {
            break;
          }
          this._executeCallback(selector, callback, target, record, Observer.MutationType.Attributes);
          break;

        case 'characterData':
          this._executeCallback(selector, callback, target, record, Observer.MutationType.CharacterData);
          break;

        case 'childList':
          // Handle when elements have been modified, instead of added and removed. This happens when elements are
          // moved, or when text of an element is modified by removing and then adding a CharacterData node.
          // (characterData event handles other cases.)
          if ((options.childList || options.modified) && record.addedNodes.length === 1 &&
              record.removedNodes.length === 1) {
            var addedNode   = record.addedNodes[0],
                removedNode = record.removedNodes[0];

            // The element has been moved.
            if (addedNode === removedNode) {
              if (!textOnly) {
                this._executeCallback(selector, callback, addedNode, record, Observer.MutationType.ElementMoved);
              }
              break;
            }

            // The CharacterData for a node has been modified.
            // The nodes text has changed.
            if (addedNode instanceof window.CharacterData && removedNode instanceof window.CharacterData &&
                addedNode.previousSibling === removedNode.previousSibling) {
              if (!elementsOnly) {
                this._executeCallback(selector, callback, target, record, Observer.MutationType.TextModified);
              }
              break;
            }
          }

          // Handle items that have been added.
          if ((options.childList || options.added) && record.addedNodes.length) {
            this._handleChildList(record, target, record.addedNodes, elementsOnly, textOnly, subtree, selector,
                                  callback, Observer.MutationType.ElementAdded, Observer.MutationType.TextAdded);
          }

          // Handle items that have been removed.
          if ((options.childList || options.removed) && record.removedNodes.length) {
            this._handleChildList(record, target, record.removedNodes, elementsOnly, textOnly, subtree, selector,
                                  callback, Observer.MutationType.ElementRemoved, Observer.MutationType.TextRemoved);
          }
          break;
      }
    }
  }

  // Start watching again.
  this.start();
};

/**
 * Process {@link MutationRecord#addedNodes} or {@link MutationRecords#removedNodes}.
 * @param {!MutationRecord} record that triggered this event.
 * @param {!Node} target Node that triggered the event.
 * @param {!NodeList} nodes The node that were mutated.
 * @param {!boolean} elementsOnly Whether to search the subtree for changes.
 * @param {!boolean} textOnly Whether to search the subtree for changes.
 * @param {!boolean} subtree Whether to search the subtree for changes.
 * @param {string} selector CSS selector to filter events with.
 * @param {!module:frontend/observer~ElementMutated} callback Callback to execute when the associated options and
 *     selector are matched in an event.
 * @param {!Number} elementType MutationType for elements.
 * @param {!Number} textDetails MutationType for text.
 * @private
 */
Observer.prototype._handleChildList =
    function (record, target, nodes, elementsOnly, textOnly, subtree, selector, callback, elementType, textType) {
      for (var i = 0, len = nodes.length; i < len; i++) {
        var node = nodes[i];

        // Handle textual changes a little differently.
        if (node instanceof window.CharacterData) {
          if (elementsOnly) continue;
          this._executeCallback(selector, callback, target, record, textType);
        } else {
          if (textOnly) continue;
          this._executeCallback(selector, callback, node, record, elementType);

          if (subtree && selector) {
            var matchingElements = node.querySelectorAll(selector);
            for (var j = 0, len2 = matchingElements.length; j < len2; j++) {
              this._executeCallback(null, callback, matchingElements[j], record, elementType);
            }
          }
        }
      }
    };

/**
 * Execute the passed callback with error handling.
 * @param {string} selector CSS selector to filter events with.
 * @param {!module:frontend/observer~ElementMutated} callback Callback to execute when the associated options
 *     and selector are matched in an event.
 * @param {!Element} element Element that has changed.
 * @param {MutationRecord} record {@link MutationRecord} for this event.
 * @param {Object} [details] Optional details to be sent to the callback.
 * @private
 */
Observer.prototype._executeCallback = function (selector, callback, element, record, details) {
  if (!selector || element.matches(selector)) {
    try {
      callback(element, record, details);
    } catch (exception) {
      console.error('Callback threw an exception.');
      console.error(exception);
      console.error(exception.stack);
    }
  }
};

/**
 * Watch for changes that match the given parameters.
 * @param {!MutationObserverInit} options MutationObserverInit options. Can also set a custom 'added',
 *     'removed', or
 *    'modified' tag to true if you are not interested in getting both events with 'childList'. Also allows you
 *     to set a 'elementOnly' or 'textOnly' to specify which changes to the childList are interesting.
 * @param {string} selector CSS selector to filter events with.
 * @param {!module:frontend/observer~ElementMutated} callback Callback to execute when the associated options
 *     and selector are matched in an event.
 */
Observer.prototype.observe = function (options, selector, callback) {
  if (!callback) {
    callback = selector;
    selector = null;
  }

  // Add the new pattern.
  this._matchers.push({options: options, selector: selector, callback: callback});

  // Restart the observer so it picks up on the new patterns.
  if (this._running) {
    this.stop();
    this.start();
  }
};

/**
 * Stop watching for changes that match the given parameters.
 * @param {!MutationObserverInit} options MutationObserverInit options. Can also set a custom 'added',
 *     'removed', or
 *    'modified' tag to true if you are not interested in getting both events with 'childList'. Also allows you
 *     to set a 'elementOnly' or 'textOnly' to specify which changes to the childList are interesting.
 * @param {string} selector CSS selector to filter events with.
 * @param {!module:frontend/observer~ElementMutated} callback Callback to execute when the associated options
 *     and selector are matched in an event.
 */
Observer.prototype.disconnect = function (options, selector, callback) {
  if (!callback) {
    callback = selector;
    selector = null;
  }

  for (var i = 0, len = this._matchers.length; i < len; i++) {
    var matcher = this._matchers[i];
    if (matcher.options === options && matcher.selector === selector && matcher.callback === callback) {
      this.patterns.splice(i);
      i--;
      len--;
    }
  }

  // If there are no more patterns, stop listening, but keep internal state as running so we will auto restart.
  if (!this._matchers.length && this._running) {
    this._observer.disconnect();
  }
};

/**
 * Stop watching for any changes.
 */
Observer.prototype.disconnectAll = function () {
  this._matchers = [];

  // Stop listening, but keep internal state as running so we will auto restart.
  if (this._running) {
    this._observer.disconnect();
  }
};

/**
 * Stop the observer.
 */
Observer.prototype.stop = function () {
  if (!this._running) {
    return;
  }
  this._observer.disconnect();
  this._running = false;
};

/**
 * Start the observer.
 */
Observer.prototype.start = function () {
  if (this._running) {
    return;
  }

  // Generate the MutationObserverInit object.
  var options = {};
  for (var i = 0, len = this._matchers.length; i < len; i++) {
    var matcherOptions = this._matchers[i].options;
    var keys = Object.keys(matcherOptions);

    for (var j = 0, len2 = keys.length; j < len2; j++) {
      var key = keys[j];
      if (key === 'attributeFilter' && options[key]) {
        Array.prototype.push.apply(options[key], matcherOptions[key]);
      } else if (key === 'added' || key === 'removed' || key === 'modified') {
        options.childList = true;
      } else {
        options[key] = options[key] || matcherOptions[key];
      }
    }
  }

  this._observer.observe(this._target, options);
  this._running = true;
};

module.exports = Observer;