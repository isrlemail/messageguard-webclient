/**
 * Created by Scott Ruoti on 4/27/2016.
 */

require('./bootstrap-tour-standalone');
var $ = require('jquery');

var MessageType = require('../../../common/message-type'),
    MouseEvents = require('./mouse-events'),
    urls        = require('../../../common/urls');

var closeButtonSrc = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAABsCAYAAACPZlfNAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA+NJREFUeNrsne1x2zAMhkVf/8cb1J2g2iAawZ2g7AYewaOoG6gbuBs4G7gbOBOoREr7XNdqSIkfAPm+d7j4LqeYwWPIAAlSahzHxldKqbX50VlrjT03kItejJ2MHYwNxvcn779AwFzNaGOsN3amS2GLjcBpLwaeoODkOEaR1gUBZrRDRCWzwdh6FjC6EFGVLdpaL2AW1hHOy2bnKWiAJQzaI2C4DfKCtp4EZhMMOIpZ6v8QmE3dkQ3ytN0jYLgVCrg1ruxUE0XXV8wcsdWT/bpqEF2CooxYKZvGnyxFiLe+rOyMO2DJ0PYCDBIErIUf5CQfyn6hQUK0ggsADAIwCMAADAIwCMAADAIwCMAADMqjDwzHRBsG3jpgjekmz9LPj+ZPq9+mYbgSz2lVtb/r4qKVhNSNQfpuDJqZj3jCygRNT4xBA5gDrMTQ9Dtj0ADmACsRNO04Bl07sN5zQ2EMaNpzDLpWYF6wIkHTM8egawN2nOOowND0wjHsawK2X+KsANB0gPff5PBdrpmO9eLicRypsO2MvXpe+s1c23P4HyQVzpM7DCNHmg70njk3PfLbFhoJWgmwsqf1qaCVAotF4RwbWkmw2ExNxYJWGixWk7+hoZUIa+TWW08pemdT9uyyh6AdjH3GivNj0WLlwTiqBSw5LQLZoXGFxbmnIxs0zrC4N+Ekh8YdFndgSaFJgCUBWBJoUmBJARYVmiRYkoBFgSYNljRgF2gho4yAbYT5QNTxPUGmmxg0qxYxl5gcllBodcMSCA2whEEDLGHQAEsSNK5pfahWtHlp8/wWuirrsKywJEAr7jZ4KYgLvT0WCevY5GtWrQJYrIaZEqEVC6tUaEXDKhFa8bBKg5bnSXSZmjxDQtM1Fc6LG0VnLj6GXAQdaiqcNxlghYbW1VY468y97rNvjzV+h809ciH0xgRvaDVnib6HmsTaReIMDXWY+7FBsbf8vAsNMx3uB3Ol2p81/ShezCU6H32XejPdP9AwW+9w6IpN+9smz87Hsy2K6cOy5bbyjKcbCRPO/AUwCMAgAAMwCMAgAAMwCMAgAAMwKCOwF7hBFrAT3CAL2AFuEKOfBGyAH8RoULSqqpSiKHuGP9jr0yVL7OEL9vpuguukbN8CRRklHx/hF77RRcBu6zANn/COrrdXdx1ClICMMFZGTUDrC6P7mQ6Ksl/4QLPS1oA639ZhV9lfUGvXK/zEQnQExt91ciFH0pVojx+TXNA5giXZ5F4Dlx0jBzgwaYLR/ZeJ4zabHaIt/i3wNhtcBOwm2vYAFwWU8zFL15kOHymltjabJHtCMuc/625r3uFaEDvqtwADAK+veBTE8C5fAAAAAElFTkSuQmCC';

/**
 * The email controller loads the pagescanner, ', and overlay
 * instances. It also registers event listeners so that it will be notified when
 * the page changes. Whenever the page changes, the controller removes old overlays
 * and instantiates new ones for the current page.
 * @class Controller
 *
 * @param overlayManagersArray - reference to array of overlay managers for the current controller.
 *
 * @constructor
 */
function Tutorial(overlayManagersArray) {

  this.currentOverlayManagers = overlayManagersArray || [];

  this.startTemplate = [
    "<div class='popover tour mg-tour'>",
    "<div class='arrow'></div>",
    "<div class='popover-title-div'>" +
    "<h3 class='popover-title'></h3>",
    "<img src='" + closeButtonSrc + "' data-role='end' title='Exit Tutorial' />" +
    "</div>" +
    "<div class='popover-content'></div>",
    "<div class='popover-navigation'>",
    "<button class='btn btn-sm btn-default' data-role='end'>Skip tutorial</button>",
    "<div class='btn-group'>",
    "<button class='btn btn-sm btn-default disabled' data-role='prev'>« Prev</button>",
    "<button class='btn btn-sm btn-default' data-role='next'>Next »</button>",
    "</div>",
    "</div>",
    "</div>"
  ].join("\n");

  this.defaultTemplate = [
    "<div class='popover tour mg-tour'>",
    "<div class='arrow'></div>",
    "<div class='popover-title-div'>" +
    "<h3 class='popover-title'></h3>",
    "<img src='" + closeButtonSrc + "' data-role='end' title='Exit Tutorial' />" +
    "</div>" +
    "<div class='popover-content'></div>",
    "<div class='popover-navigation'>",
    "<div class='btn-group'>",
    "<button class='btn btn-sm btn-default' data-role='prev'>« Prev</button>",
    "<button class='btn btn-sm btn-default' data-role='next'>Next »</button>",
    "</div>",
    "</div>",
    "</div>"
  ].join("\n");

  this.disabledPrev = [
    "<div class='popover tour mg-tour'>",
    "<div class='arrow'></div>",
    "<div class='popover-title-div'>" +
    "<h3 class='popover-title'></h3>",
    "<img src='" + closeButtonSrc + "' data-role='end' title='Exit Tutorial' />" +
    "</div>" +
    "<div class='popover-content'></div>",
    "<div class='popover-navigation'>",
    "<div class='btn-group'>",
    "<button class='btn btn-sm btn-default disabled' data-role='prev'>« Prev</button>",
    "<button class='btn btn-sm btn-default' data-role='next'>Next »</button>",
    "</div>",
    "</div>",
    "</div>"
  ].join("\n");

  this.finishedTemplate = [
    "<div class='popover tour mg-tour'>",
    "<div class='arrow'></div>",
    "<div class='popover-title-div'>" +
    "<h3 class='popover-title'></h3>",
    "<img src='" + closeButtonSrc + "' data-role='end' title='Exit Tutorial'/>" +
    "</div>" +
    "<div class='popover-content'></div>",
    "<div class='popover-navigation'>",
    "<div class='btn-group'>",
    "<button class='btn btn-sm btn-default' data-role='prev'>« Prev</button>",
    "<button class='btn btn-sm btn-default' data-role='end'>Done</button>",
    "</div>",
    "</div>",
    "</div>"
  ].join("\n");

  this.actionTemplate = [
    "<div class='popover tour mg-tour'>",
    "<div class='arrow'></div>",
    "<div class='popover-title-div'>" +
    "<h3 class='popover-title'></h3>",
    "<img src='" + closeButtonSrc + "' data-role='end' title='Exit Tutorial' />" +
    "</div>" +
    "<div class='popover-content'></div>",
    "</div>"
  ].join("\n");

  /**
   * Whether to enable listeners for the other tutorials on exit.
   */
  this.enableListenersOnFinish = false;

  /**
   * Tracks whether the listeners have already been setup or not.
   */
  this.alreadySetup = false;

  /**
   * Keeps track of the current tour.
   */
  this.currentTour = null;

  /**
   * Tutorial initializer
   * @method initialize
   */
  var self = this;

  var defaultTourOptions = {
    // Allow generic instructions that do not highlight a specific item.
    orphan: true,

    // Only allow interaction with the intended icon.
    backdrop: true,

    // Turn off storage, so the tutorial always starts from the begining.
    storage: false,

    // Turn off keyboard navagation of the tutorial.
    keyboard: false,

    // HTML template for the tutorial.
    template: this.defaultTemplate
  };

  // Create the base tour.
  this.baseTutorial = new window.Tour($.extend({}, defaultTourOptions, {
    name:     'baseTutorial',
    steps:    [{
      title: 'Welcome!',

      content:  '<p>MessageGuard is now protecting Gmail.</p>',
      template: this.finishedTemplate,
      onShown:  function () { },
      onHidden: function () { }
    }],
    onEnd:    function (tour) {
      self.currentTour = null;
      if (self.enableListenersOnFinish) {
        self.setupListeners();
      }
      self.enableListenersOnFinish = false;
      localStorage.setItem('priorMessageGuardUserBase', true);
    },
    onShown:  function (tour) {
      self.updateTutorialMask();
    },
    onHidden: function (tour) {
      self.removeUpdatedTutorialMask();
    }
  }));

  // Create the Compose tour.
  this.composeTutorial = new Tour($.extend({}, defaultTourOptions, {
    name:     'composeTutorial',
    steps:    [{
      element: 'div[role="button"]:contains("COMPOSE"):eq(0)',

      title:    'Click compose',
      content:  '<p>Click the compose button to begin.</p>',
      template: this.actionTemplate,

      // Move to the next step when the compose button is clicked.
      onShown:  function (tour) {
        tour.checkForComposeInterfaceInterval = window.setInterval(function () {
          if ($('.messageGuardBar:visible').length > 0) {
            window.clearInterval(tour.checkForComposeInterfaceInterval);
            tour.checkForComposeInterfaceInterval = null;
            tour.next();
          }
        }, 1000);
      },
      onHidden: function (tour) {
        window.clearInterval(tour.checkForComposeInterfaceInterval);
      }
    }, {
      element:   '[role="dialog"] .messageGuardBar:visible:last',
      placement: function () { return self.pickValueBasedOnMaximized('bottom', 'left'); },

      title:    'Click "Turn on encryption"',
      content:  '<p>Clicking "Turn on encryption" will encrypt this email.</p>' +
                '<p>Click this <i>before</i> typing your email to prevent Gmail from reading it while you compose it.</p>',
      template: this.actionTemplate,

      // Move to the next step when the compose button is clicked.
      onShown:  function (tour) {
        self.updateTutorialMask();

        tour.checkForComposeWindowInterval = window.setInterval(function () {
          if ($('.messageGuardComposeOverlayed:visible').length > 0) {
            window.clearInterval(tour.checkForComposeWindowInterval);
            tour.checkForComposeWindowInterval = null;
            window.setTimeout(function () { tour.next(); }, 50);
          }
        }, 10);
      },
      onHidden: function (tour) {
        self.removeUpdatedTutorialMask();

        window.clearInterval(tour.checkForComposeWindowInterval);
      }
    }, {
      element:   '[role="dialog"] .fX.aXjCH:visible:last,[role="dialog"] .hl:visible:last',
      placement: function () { return self.pickValueBasedOnMaximized('bottom', 'left'); },

      title:    'Add recipients',
      content:  '<p>This email can only be read by the recipients you specify.</p>' +
                '<p>Even Gmail cannot read your encrypted email.</p>',
      template: this.disabledPrev,

      // Move to the next step when the Next button is clicked.
      onShown: function (tour) {
        self.updateTutorialMask();
        $(tour._options.steps[tour._current].element).closest('form').find('.fX.aXjCH,.hl').addClass(
            'tour-step-backdrop');
      },
      onHide:  function (tour) {
        $(tour._options.steps[tour._current].element).closest('form').find('.tour-step-backdrop').removeClass(
            'tour-step-backdrop');
      }
    }, {
      element:   '[role="dialog"] .aoD.az6:not(.composePreamble):visible:last',
      placement: function () { return self.pickValueBasedOnMaximized('bottom', 'left'); },

      title:   'Enter a subject',
      content: 'The subject is not encrypted, so avoid putting anything sensitive here.</p>',
    }, {
      element:   '[role="dialog"] .aoD.az6.composePreamble:visible:last',
      placement: function () { return self.pickValueBasedOnMaximized('bottom', 'left'); },

      title:   'Add an optional greeting',
      content: '<p>MessageGuard adds an unencrypted \'greeting\' field, so you can help recipients who don\'t use MessageGuard to know this email isn\'t spam.</p>'
    }, {
      element:   '[role="dialog"] .messageGuardComposeOverlayed:visible:last',
      placement: function () { return self.pickValueBasedOnMaximized('top', 'left'); },

      title:   'Write your message',
      content: '<p>Anything written in this box is encrypted.</p>'
    }, {
      element:   '[role="dialog"] .messageGuardComposeOverlayed:visible:last',
      placement: function () { return self.pickValueBasedOnMaximized('top', 'left'); },

      title:    'Encryption keys',
      content:  '<p>This is where you can see and select which key will be used to encrypt your message.</p>',

      onShown: function (tour) {
        self.updateTutorialMask();
        self.postMessageToOverlay(MessageType.TUTORIAL_MASK_PLACE);
      },
      onHide:  function (tour) {
        self.removeUpdatedTutorialMask();
        self.postMessageToOverlay(MessageType.TUTORIAL_MASK_REMOVE);
      }
    }, {
      title:    'Instructions',
      content:  '<p>Encrypted email automatically includes instructions on how to decrypt it.</p>',
      template: this.finishedTemplate
    }],
    onStart:  function (tour) {
      localStorage.setItem('priorMessageGuardUserCompose', true);
    },
    onEnd:    function (tour) {
      self.currentTour = null;
    },
    onShown:  function (tour) {
      self.updateTutorialMask();
    },
    onHidden: function () {
      self.removeUpdatedTutorialMask();
    }
  }));

  this.readTutorial = new Tour($.extend({}, defaultTourOptions, {
    name:    'readTutorial',
    steps:   [{
      title:   'Open an encrypted email',
      content: '<p>Open an encrypted email to begin.</p>',

      template: this.actionTemplate,
      backdrop: false,

      onShown:  function (tour) {
        tour.checkForReadWindowInterval = window.setInterval(function () {
          if ($('.messageGuardReadOverlayed:visible').length > 0) {
            window.clearInterval(tour.checkForReadWindowInterval);
            tour.checkForReadWindowInterval = null;
            tour.next();
          }
        }, 10);
      },
      onHidden: function (tour) {
        window.clearInterval(tour.checkForReadWindowInterval);
      }
    }, {
      element:   '.messageGuardLockDiv:visible:first',
      placement: 'bottom',

      title:    'Encrypted label',
      content:  '<p>This label means an email is encrypted.</p>',
      template: this.startTemplate,
    }, {
      element:   '.ix td:nth-child(1) h3:nth-child(1):visible:first',
      placement: 'bottom',

      title:   'Verified sender',
      content: '<p>MessageGuard verifies that an email is really from the address it says it is.</p>',
    }, {
      element:   '.adz .ajw:visible:first',
      placement: 'bottom',

      title:   'Only readable by recipients',
      content: '<p>An encrypted email can only be read by its recipients.</p>' +
               '<p>Even Gmail cannot read your encrypted email.</p>',

      onHidden: function (tour) {
        // Record which step we left, so the following (preamble) step knows if it was arrived using the next or
        // previous buttons.
        tour.prevStep = tour._current;
      }
    }, {
      element:   'div[name=preamble]:visible:first',
      placement: 'top',

      title:   'Unencrypted greeting',
      content: '<p>Authors can include an unencrypted greeting to give context.</p>',

      onShown: function (tour) {
        // If there is no preamble skip this step. Use the prevStep variable to know which direction we are traveling.
        if ($('div[name=preamble]:visible').length == 0) {
          if (tour.prevStep == tour._current - 1) {
            tour.goTo(tour._current + 1);
          } else {
            tour.goTo(tour._current - 1);
          }
        }
      }
    }, {
      element:   '.messageGuardReadOverlayed:visible:first',
      placement: 'top',

      title:   'Encrypted email',
      content: '<p>Messageguard shows decrypted text within this box, and ensures that it hasn\'t been tampered with.</p>',

      onHidden: function (tour) {
        // Record which step we left, so the following (preamble) step knows if it was arrived using the next or
        // previous buttons.
        tour.prevStep = tour._current;
      }
    }, {
      element:   '.messageGuardReadOverlayed:visible:first',
      placement: 'top',

      title:   'Encryption key',
      content: '<p>This is the key for the person that sent you this message.</p>' +
               '<p>Hover over this key to verify the email address of the sender.</p>',

      onShown:  function (tour) {
        self.updateTutorialMask();
        self.postMessageToOverlay(MessageType.TUTORIAL_MASK_PLACE);
      },
      onHidden: function (tour) {
        self.removeUpdatedTutorialMask();
        self.postMessageToOverlay(MessageType.TUTORIAL_MASK_REMOVE);
      }
    }, {
      title:    'Encrypted replies',
      content:  '<p>Replies to encrypted email are automatically encrypted.</p>' +
                '<p>To protect the original email contents, encryption cannot be turned off for replies.</p>',
      template: this.finishedTemplate
    }],
    onStart: function (tour) {
      localStorage.setItem('priorMessageGuardUserRead', true);
      if ($('.messageGuardReadOverlayed:visible').length > 0) {
        tour.goTo(1);
      }
    },
    onEnd:   function (tour) {
      self.currentTour = null;
    }
  }));

  this.inboxTutorial = new Tour($.extend({}, defaultTourOptions, {
    name:     'inboxTutorial',
    steps:    [{
      element:   '.gbqfqw:visible:eq(0)',
      placement: 'bottom',

      title:   'Finding encrypted email',
      content: '<p>To find encrypted email, search for <span style="white-space: pre; font-family: monospace;">subject: encrypted</span> and any words in the subject or greeting.</p>' +
               '<p>Because these email are encrypted, you can\'t find them by searching for their contents.</p>'
    }, {
      element:   '.messageGuardLockDiv:visible:first',
      placement: 'bottom',

      title:    'Encrypted label',
      content:  '<div class="messageGuardLockDiv" style="display: none">Encrypted</div>' +
                '<p>The above label means an email is encrypted.</p>' +
                '<p>MessageGuard must be running to read encrypted email.</p>',
      template: this.finishedTemplate,
      onShown:  function (tour) {
        self.updateTutorialMask();

        if ($(tour._options.steps[tour._current].element).length == 0) {
          $('.popover.tour .messageGuardLockDiv').show();
        }
      },
    }],
    onStart:  function (tour) {
      localStorage.setItem('priorMessageGuardUserInbox', true);
      window.location.hash = '#search/subject%3A+encrypted';
    },
    onEnd:    function (tour) {
      self.currentTour = null;
    },
    onShown:  function () {
      self.updateTutorialMask();
    },
    onHidden: function () {
      self.removeUpdatedTutorialMask();
    }
  }));

  this.baseTutorial.init();
  this.composeTutorial.init();
  this.readTutorial.init();
  this.inboxTutorial.init();
};

Tutorial.prototype.initialRun = function () {
  if (!localStorage.priorMessageGuardUserBase) {
    this.enableListenersOnFinish = true;
    this.playBaseTutorial();
  } else {
    this.setupListeners();
  }
};

/**
 * Play the base tutorial.
 */
Tutorial.prototype.playBaseTutorial = function () {
  if (this.currentTour != null) {
    return;
  }
  this.currentTour = this.baseTutorial;

  this.baseTutorial.restart();
};

/**
 * Walks the user through the process of using MessageGuard to compose a message.
 * @method playComposeTutorial
 */
Tutorial.prototype.playComposeTutorial = function (skipToStepTwo) {
  if (this.currentTour != null) {
    return;
  }
  this.currentTour = this.composeTutorial;

  var self = this;
  this.minimizeComposeWindows();

  // Play the tutorial.
  if (skipToStepTwo) {
    var checkForComposeInterfaceInterval = window.setInterval(function () {
      if ($('.messageGuardBar:visible').length > 0) {
        window.clearInterval(checkForComposeInterfaceInterval);
        checkForComposeInterfaceInterval = null;

        self.composeTutorial.restart();
        self.composeTutorial.goTo(1);
      }
    }, 10);
  } else {
    this.composeTutorial.restart();
  }
};

Tutorial.prototype.playReadTutorial = function () {
  if (this.currentTour != null) {
    return;
  }
  this.currentTour = this.readTutorial;

  this.minimizeComposeWindows();
  this.readTutorial.restart();
};

Tutorial.prototype.playInboxTutorial = function () {
  if (this.currentTour != null) {
    return;
  }
  this.currentTour = this.inboxTutorial;

  var self = this;
  this.minimizeComposeWindows();

  self.inboxTutorial.restart();
};

/**
 * Minimize the compose windows.
 */
Tutorial.prototype.minimizeComposeWindows = function () {
  $('div.nH.Hd').filter(function () { return $(this).children('div.nH:nth-child(3):visible').length; }).find(
      '.l.m').each(function () { MouseEvents.simulateMouseClick(this) });
};

/**
 * Update the tutorial mask position to work better in gmail.
 */
Tutorial.prototype.updateTutorialMask = function () {
  var stepElement = $(this.currentTour._options.steps[this.currentTour._current].element);
  stepElement.after($('.tour-step-background'));
  stepElement.after($('.tour-backdrop'));


  // Cover up the gmail icon.
  var gmailIcon = $('div.akh');
  if (!gmailIcon.is(stepElement) && gmailIcon.has(stepElement).length == 0) {
    gmailIcon.css('z-index', 0);
  }

  // Cover up the banner.
  var banner = $('div[role=banner]');
  var bannerHeight = banner.height();
  var backdrop = $('.tour-backdrop');
  backdrop.css('top', bannerHeight);

  // Cover everything but the current element.
  var extraDivHTML = '<div class="messageGuardExtraMask tour-backdrop"></div>';
  if (banner.is(stepElement)) {
    return;
  } else if (banner.has(stepElement).length > 0) {
    var offset = stepElement.offset();

    var leftDiv = $(extraDivHTML)
        .height(bannerHeight)
        .width(offset.left);

    var rightDiv = $(extraDivHTML)
        .height(bannerHeight)
        .css('left', offset.left + stepElement.width());

    var topDiv = $(extraDivHTML)
        .height(offset.top - 1)
        .width(stepElement.width())
        .css('top', 0)
        .css('left', offset.left);

    var bottomDiv = $(extraDivHTML)
        .height(bannerHeight - offset.top - stepElement.height() + 1)
        .width(stepElement.width())
        .css('top', offset.top + stepElement.height())
        .css('left', offset.left);

    $(document.body).append(leftDiv, rightDiv, topDiv, bottomDiv);
  } else {
    var extraDiv = $(extraDivHTML);
    extraDiv.height(bannerHeight);
    $(document.body).append(extraDiv);
  }
};

/**
 * Remove the div elements created during updateTutorialMask.
 */
Tutorial.prototype.removeUpdatedTutorialMask = function () {
  $('div.akh').css('z-index', '');
  $('.messageGuardExtraMask').remove();
};

/**
 * Pick the value based on if the current element is maximized or not. Return value1 if it is, value 2 otherwise.
 */
Tutorial.prototype.pickValueBasedOnMaximized = function (value1, value2) {
  var stepElement = this.currentTour._options.steps[this.currentTour._current].element;
  return $(stepElement).parents('.aSt').length == 0 ? value2 : value1;
};

/**
 * Post message to the currently-highlighted overlay, if it can be found.
 */
Tutorial.prototype.postMessageToOverlay = function (message, data) {
  var stepElement = $(this.currentTour._options.steps[this.currentTour._current].element);

  if (!stepElement.length) {
    return;
  }

  // For each current manager, test to see if we're on its item,
  // or if we're on an ancestor of its item.
  for (var i = 0; i < this.currentOverlayManagers.length; i++) {
    var manager = this.currentOverlayManagers[i];
    if (stepElement.is(manager.item) ||
        $.contains(stepElement[0], manager.item) ||
        $.contains(manager.item, stepElement[0])) {

      manager.postMessage(message, data);
    }
  }
};

/**
 * Setup listeners for the tutorials.
 */
Tutorial.prototype.setupListeners = function () {
  var self = this;

  if (this.alreadySetup) {
    return;
  }
  this.alreadySetup = true;

  // Listen for first compose action after using MessageGuard.
  if (!localStorage.priorMessageGuardUserCompose) {
    self.waitForElement('div[role="button"]:contains("COMPOSE")').then(function (composeButton) {
      var onComposeClick = function () {
        composeButton.off('click', onComposeClick);

        if (!localStorage.priorMessageGuardUserCompose) {
          localStorage.setItem('priorMessageGuardUserCompose', true);
          if (confirm(
                  "This is the first time you have composed an email with MessageGuard. Would you like to see a tutorial on how to encrypt this message?")) {
            self.playComposeTutorial(true);
          }
        }
      };
      composeButton.on('click', onComposeClick);
    });
  }

  // Listen for first read dialog after installing MessageGuard.
  if (!localStorage.priorMessageGuardUserRead) {
    self.waitForElement('.messageGuardReadOverlayed:visible').then(function () {
      self.playReadTutorial();
    });
  }
};

Tutorial.prototype.waitForElement = function (selector) {
  return new Promise(function (resolve) {

    var intervalID = window.setInterval(function () {

      var elem = $(selector);

      if (elem.length > 0) {
        window.clearInterval(intervalID);
        resolve(elem);
      }

    }, 100);

  });
};

module.exports = Tutorial;
