/**
 * Created by Scott Ruoti on 4/26/2016.
 */

var jQuery  = require('jquery'),
    util    = require('util'),
    XRegExp = require('xregexp');

var encoder = require('../../../common/encoder');

/**
 * Handles the wraping of the body and subject of kiwi packages.
 *
 * @class EmailPackageWrapper
 * @extends PackageWrapper
 */
module.exports = (function () {

  //#region Private constants
  /**
   * An optional preamble template for user supplied preamble to the bootstrap message.
   * @property preamble
   * @type String
   */
  var preambleHTML = [
    "<div name='preamble'>",
    "@PREAMBLE",
    "</div>",
  ].join('\n');

  var messageGuardDescriptionHTML = [
    '<div style="font-size:24px;text-align:center;font-weight:bold;">This message was encrypted using MessageGuard.</div>',
    '<div style="min-height:35px;clear:both;"></div>',
    '<div style="font-size:24px;display:block;margin: 0 auto;width:373px;text-align:center;">',
    '  <a href="https://keys.messageguard.io" style="text-decoration:none" target="_blank">',
    '    <div style="background-color:#2c3e50;border:1px solid #233140;border-radius:5px;padding:10px;color:white;">',
    '      Click here to get MessageGuard',
    '    </div>',
    '  </a>',
    '</div>',
    '<div style="min-height:60px"></div>',
    '<div style="font-size: 14pt;line-height:0px;font-weight: bold;">What does this mean?</div>',
    '<br>',
    '<br>',
    'The sender of this message encrypted it using MessageGuard. This protects messages from malicious attackers and keeps it private from your webmail provider. You will need to install MessageGuard in order to decrypt this message.',
    '<br>',
    '<br>',
    'For more information, please visit <a href="https://messageguard.io">https://messageguard.io</a>.',
    '<br>',
    '<br>',
    '<br>',
    '<br>'
  ].join('\n');

  var PackageWrapperHTML = [
    "<div>",
    "<div>",
    "---Begin MessageGuard Encrypted Message---",
    "</div>",
    "<br />",

    "<div name='package' style='font-family:monospace; font-size: 11px;'>@PACKAGE</div>",
    "</div>",
    "<br />",

    "<div style='display: block;'>",
    "---End MessageGuard Encrypted Message---",
    "</div>",
    "</div>"
  ].join('\n');

  /**
   * The header for a MessageGuard subject.
   * @property subjectHeader
   * @type String
   */
  var subjectHeader = "[MessageGuard]";

  /**
   * The regex used to match subjects.
   * @property subjectRegex
   * @type XRegExp
   */
  var subjectRegex = new XRegExp(util.format(
      "^\\s*(?<MessageType>((?<Reply>re:)|(?<Forward>fwd:))?\\s*)?%s(?<Subject>.*)",
      XRegExp.escape(subjectHeader)), "i");

  //#endregion

  function PackageWrapper() {

    //#region Fields

    /**
     * The subject of the message.
     * @property subject
     * @type String
     */
    this.subject = null;

    /**
     * The wrapped subject of the message.
     * @property wrappedSubject
     * @type String
     */
    this.wrappedSubject = null;

    /**
     * The package that is actually being sent in the message.
     * @property package
     * @type Package
     */
    this.package = null;

    /**
     * The body of the message that is wrapping the subject.
     * @property wrappedBody
     * @type String
     */
    this.wrappedBody = null;

    //#endregion

  }

  //#region Body Methods

  /**
   * Wraps the package with an optional preamble message.
   * @method wrapPackage
   * @param {string} preambleMessage An optional pre-amble set by the user for this message.
   */
  PackageWrapper.prototype.wrapPackage = function (preambleMessage) {
    this.wrappedBody = '<div name="messageGuardPackage">';
    if (preambleMessage != null && preambleMessage.trim() != "") {
      preambleMessage = preambleMessage.replace(/(?:\r\n|\r|\n)/g, '<br />');
      this.wrappedBody += preambleHTML.replace('@PREAMBLE', preambleMessage);
      this.wrappedBody += "<br /><hr /><br />";
    }

    this.wrappedBody += '<div name="encryptedEmail">';
    this.wrappedBody += messageGuardDescriptionHTML;
    this.wrappedBody += "<br /><hr /><br /><br />";

    var packageText    = JSON.stringify(this.package),
        encodedPackage = encoder.convert(packageText, 'utf8', 'base64');

    this.wrappedBody += PackageWrapperHTML.replace('@PACKAGE', encodedPackage);
    this.wrappedBody += '</div></div>';
  };

  /**
   * Unwraps the package from a message.
   * @method unwrapPackage
   */
  PackageWrapper.prototype.unwrapPackage = function () {
    var root = document.createElement('div');
    root.innerHTML = this.wrappedBody;
    var packagedText = jQuery('div[name="package"]', root).text();

    this.package = JSON.parse(encoder.convert(packagedText, 'base64', 'utf8'));
    this.preamble = PackageWrapper.getPreamble(this.wrappedBody);
  };

  //#endregion


  //#region Simple static package operations

  /**
   * Checks whether a given subject is a wrapped subject.
   * @method isWrappedSubject
   * @param {String} subject The subject of the message.
   * @return {boolean} Whether the subject is wrapped or not.
   */
  PackageWrapper.isWrappedSubject = function (subject) {
    return subjectRegex.test(subject);
  };

  /**
   * Checks whether the given body has a wrapped pacakge.
   * @method hasWrappedPackage
   * @param {String} body The body of the message.
   * @return {boolean} Whether there is a wrapped package or not.
   */
  PackageWrapper.hasWrappedPackage = function (body) {
    var root = document.createElement('div');
    root.innerHTML = body;
    return jQuery('div[name="encryptedEmail"]', root).length > 0;
  };

  /**
   * Gets the preamble for the message.
   * @method getPreamble
   * @param {String} body The body of the message.
   * @return {String} The preamble, if there was one.
   */
  PackageWrapper.getPreamble = function (body) {
    var root = document.createElement('div');
    root.innerHTML = body;
    var preamble = jQuery('div[name="preamble"]', root);
    if (preamble.length > 0) {
      return preamble[0].innerHTML;
    } else {
      return null;
    }
  };

  /**
   * Unwraps the given subject.
   * @method unwrapSubject
   * @param {String} wrappedSubject The wrapped subject.
   * @return {String} The unwrapped package.
   */
  PackageWrapper.unwrapSubject = function (wrappedSubject) {
    if (!PackageWrapper.isWrappedSubject(wrappedSubject)) {
      return wrappedSubject;
    }
    var results = XRegExp.exec(wrappedSubject, subjectRegex);
    return ((results.MessageType == null ? "" : results.MessageType) + results.Subject).trim();
  };

  /**
   * Unwraps the given body.
   * @method unwrapPackage
   * @param {String} wrappedBody The body with a wrapped subject.
   * @return {String} The unwrapped package.
   */
  PackageWrapper.unwrapPackage = function (wrappedBody) {
    var wrapper = new PackageWrapper();
    wrapper.wrappedBody = wrappedBody;
    try {
      wrapper.unwrapPackage();
      return wrapper.package;
    }
    catch (e) {
      return null;
    }
  };

  /**
   * Gets a template of a body with a package, where the package and its surrounding
   * text is replaced by the given template name.
   * @method getTemplate
   * @param {String} body The body with a wrapped package.
   * @param {String} template The template to insert in place of the package.
   * @return {String} The body with the kiwi message replaced by the template.
   */
  PackageWrapper.getTemplate = function (body, template) {
    var messageBody = document.createElement('div');
    messageBody.innerHTML = body;
    jQuery('div[name="messageGuardPackage"]', messageBody).replaceWith(template);
    return messageBody.innerHTML;
  };

  //#endregion

  //#region Static initilaize functions

  /**
   * Creates a package wrapper for a wrapper intended to write messages.
   * @method createWriteWrapper
   * @param {String} subject The subject of the message.
   * @param {String} preambleMessage An optional pre-amble to be included with the bootstrap message.
   * @param {String} package The package to be wrapped.
   * @return {PackageWrapper} The created wrapper.
   */
  PackageWrapper.createWriteWrapper = function (subject, preambleMessage, package) {
    var wrapper = new PackageWrapper();

    // Set the wrapped subject.
    wrapper.subject = subject;
    var results = subjectRegex.exec(subject);
    if (results != null) {
      wrapper.wrappedSubject = subject;
    } else {
      wrapper.wrappedSubject = util.format("%s %s", subjectHeader, subject);
    }

    // Set the package and then wrap the body.
    wrapper.package = package;
    wrapper.wrapPackage(preambleMessage);

    return wrapper;
  };

  /**
   * Creates a package wrapper for a wrapper intended to unwrap messages.
   * @method createWriteWrapper
   * @param {String} wrappedSubject The wrapped subject of the message.
   * @param {String} wrappedBody A wrapped body containing a package.
   * @return {PackageWrapper} The created wrapper, or null if the message could not be decoded.
   */
  PackageWrapper.createReadWrapper = function (wrappedSubject, wrappedBody) {
    // Check for some basic assumptions.
    if (wrappedSubject == null || wrappedBody == null || !PackageWrapper.hasWrappedPackage(wrappedBody)) {
      return null;
    }

    // Handle the subject.
    var wrapper = new PackageWrapper();
    wrapper.wrappedSubject = wrappedSubject;
    wrapper.subject = PackageWrapper.unwrapSubject(wrappedSubject);

    wrapper.wrappedBody = wrappedBody;
    try {
      wrapper.unwrapPackage();
    }
    catch (e) {
      return null;
    }

    return wrapper;
  };

  //#endregion

  return PackageWrapper;
})();
