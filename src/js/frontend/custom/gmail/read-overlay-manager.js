/**
 * Generic read overlay manager.
 * @module
 * @requires frontend/overlay-manager-base
 * @requires common/message-type
 */
"use strict";

var OverlayManagerBase = require('../../overlay-manager-base'),
    MessageType        = require('../../../common/message-type'),
    PackageWrapper     = require('./package-wrapper'),
    PageScanner        = require('./page-scanner');

var jQuery = require('jquery');

//noinspection JSClosureCompilerSyntax
/**
 * Create a read overlay manager for the given item.
 * @param {!Element} item Item that is being overlayed.
 * @constructor
 * @extends module:frontend/overlay-manager-base
 * @alias module:frontend/generic/read-overlay-manager
 */
function ReadOverlayManager(item, windowUUID) {
  OverlayManagerBase.call(this, item, windowUUID);
  this.overlayURL = 'custom/gmail/gmail-read.html';

  // Change the lock icon in the subject line.
  var subjectLine = jQuery('.hP:first');
  if (subjectLine.length) {
    if (PackageWrapper.isWrappedSubject(subjectLine.text())) {
      subjectLine.text(PackageWrapper.unwrapSubject(subjectLine.text()));
      var lockButton = document.createElement('div');
      lockButton.className = 'messageGuardLockDiv';
      lockButton.innerText = 'Encrypted';
      subjectLine.before(lockButton);
    }
  }

  // Hide the encryption-not supported icons from Gmail.
  jQuery('span[role="button"].bcU', this.item).hide();
}

ReadOverlayManager.prototype = Object.create(OverlayManagerBase.prototype);
ReadOverlayManager.prototype.constructor = ReadOverlayManager;

/**
 * Setup and begin operating.
 */
ReadOverlayManager.prototype.setup = function () {
  OverlayManagerBase.prototype.setup.call(this);
  this.placeOverlay();
};

/**
 * Called when the overlay is ready to communicate.
 */
ReadOverlayManager.prototype.onOverlayReady = function () {
  this.registerMessageHandler(MessageType.SIZING_INFO, false, function (data) {
    jQuery(this.item).height(data + 40).css({overflow: "hidden"});
  });

  this.windowResizeFunction = function () {
    this.postMessage(MessageType.SIZING_INFO);
  }.bind(this);
  jQuery(window).on('resize', this.windowResizeFunction);


  this.postMessage(MessageType.SEND_CONTENTS, {
    package: PackageWrapper.unwrapPackage(jQuery(this.item).html()),
    userId:  PageScanner.getEmailAddress()
  });


  jQuery(this.item).addClass('messageGuardReadOverlayed');

  this.postMessage(MessageType.SIZING_INFO);
};

/**
 * Cleanup this overlay manager.
 */
ReadOverlayManager.prototype.cleanup = function () {
  OverlayManagerBase.prototype.cleanup.call(this);
  jQuery(window).off('resize', this.windowResizeFunction);
};


module.exports = ReadOverlayManager;
