/**
 * Created by Scott Ruoti on 4/26/2016.
 */

var jQuery = require('jquery');

var emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/i;

module.exports = {
  getEmailAddress: function () {
    // GLOBALS is an array of interesting information that Google provides.
    try {
      return window.GLOBALS[10];
    } catch (e) {
      titleSegments = jQuery('title').html().split(" ");
      for(var i = titleSegments.length - 1; i >= 0; i--) {
        if (titleSegments[i].match(emailRegex))
          return titleSegments[i];
      }
      //return jQuery('.gb_ub').text().match(emailRegex)[0];
    }
  }
};