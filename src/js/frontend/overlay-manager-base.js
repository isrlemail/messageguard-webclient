/**
 * Abstract base overlay manager class. This class handles placing the main iFrame-based overlay when asked. It also
 * sets up messaging with the overlay.
 * @module
 * @requires frontend/strings
 * @requires frontend/overlayer
 * @requires common/urls
 * @requires common/selectors
 * @requires common/communicator-mixin
 */
"use strict";

var Errors            = require('../common/errors'),
    selectors         = require('../common/selectors'),
    stringStyles      = require('fs').readFileSync(__dirname + '/../../../build/staging/sass/css/frontend.css', 'utf8'),
    Overlayer         = require('./overlayer'),
    urls              = require('../common/urls'),
    UUID              = require('../common/uuid'),
    communicatorMixin = require("../common/communicator-mixin");

/**
 * Creates an overlay for the given item.
 * @param {!Element} item The item this overlay manager is responsible for.
 * @constructor
 * @alias module:frontend/overlay-manager-base
 */
function OverlayManagerBase(item, windowUUID) {
  /**
   * The item this overlay manager is responsible for.
   * @type {!Element}
   * @protected
   */
  this.item = item;

  /**
   * The window UUID for intra-window communication.
   * @type {string}
   * @protected
   */
  this.windowUUID = windowUUID;

  /**
   * Overlayer used by this overlay manager to position overlays.
   * @type {module:frontend/overlayer}
   * @protected
   */
  this.overlayer = new Overlayer(this.item);

  /**
   * The overlay element.
   * @type {Element}
   * @private
   */
  this._overlay = null;

  /**
   * The overlay URL, relative to the overlays directory.
   * @type {!string}
   */
  this.overlayURL = null;

  /**
   * An instance UUID to assist with debugging.
   * @type {string}
   */
  this.instanceUUID = UUID();

  communicatorMixin.call(this);
}

/**
 * Setup and begin operating.
 */
OverlayManagerBase.prototype.setup = function () {
  this.overlayer.setup(stringStyles);

  this.host = this;

  this.registerMessageHandler("change_background_color", false, function (data) {
    document.body.style.background = data;
  });
};

/**
 * Places the overlay into the page.
 * @protected
 */
OverlayManagerBase.prototype.placeOverlay = function () {
  this._overlay = document.createElement('iframe');
  this._overlay.src = urls.getOverlayUrl(this.overlayURL, {window_uuid: this.windowUUID});
  this._overlay.style.border = "0";

  var self = this;
  this._overlay.addEventListener('load', function () {
    this.channel = new MessageChannel();
    this.messagePort = this.channel.port1;
    this.messagePort.onmessage = this.handleMessage.bind(this);

    self._overlay.contentWindow.postMessage('initiate connection', '*', [self.channel.port2]);
    self.onOverlayReady();
  }.bind(this), false);

  this.overlayer.placeOverlay(this._overlay, Overlayer.OverlayPositions.Fill);
};

/**
 * Removes the overlay.
 * @protected
 */
OverlayManagerBase.prototype.removeOverlay = function () {
  if (!this._overlay) return;
  this.overlayer.removeOverlay();

  this.messagePort.onmessage = null;
  this._overlay = this.messagePort = null;
};

/**
 * Called when the overlay has been moved and needs to updated.
 */
OverlayManagerBase.prototype.onMoved = function () {
  this.overlayer.onItemMoved();
};

/**
 * Called when the overlay is ready to communicate.
 * @abstract
 */
OverlayManagerBase.prototype.onOverlayReady = function () {
  throw new Errors.AbstractMethodError("Abstract method not implemented");
};

/**
 * Checks whether this manager is responsible for this item.
 * @param {Element!} item The item to check if this overlay manager's item is the same or an ancestor.
 * @return {boolean}
 */
OverlayManagerBase.prototype.isManagerFor = function (item) {
  return this.item === item || selectors.isAncestor(item, this.item);
};

/**
 * Checks whether this manager's item is a decendant of the passed item or if the two are equal.
 * @param {Element!} item The item to check if this overlay manager's item is a decendant.
 * @return {boolean}
 */
OverlayManagerBase.prototype.isDecendant = function (item) {
  return selectors.isAncestor(this.item, item);
};

/**
 * Checks whether this manager's item is the passed item.
 * @param {Element!} item The item to check if this overlay manager's item is the same.
 * @return {boolean}
 */
OverlayManagerBase.prototype.isSelf = function (item) {
  return this.item === item;
};

/**
 * Cleanup this overlay manager.
 */
OverlayManagerBase.prototype.cleanup = function () {
  this.overlayer.cleanup();
};

module.exports = OverlayManagerBase;
