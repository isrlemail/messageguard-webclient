/**
 * Generic compose overlay manager.
 * @module
 * @requires frontend/overlayer
 * @requires frontend/overlay-manager-base
 * @requires common/message-type
 */
"use strict";

var Overlayer          = require('../overlayer'),
    OverlayManagerBase = require('../overlay-manager-base'),
    MessageType        = require('../../common/message-type');

//noinspection JSClosureCompilerSyntax
/**
 * Create a compose overlay manager for the given item.
 * @param {!Element} item Item that is being overlayed.
 * @constructor
 * @extends module:frontend/overlay-manager-base
 * @alias module:frontend/generic/compose-overlay-manager
 */
function ComposeOverlayManager(item, windowUUID) {
  OverlayManagerBase.call(this, item, windowUUID);
  this.overlayURL = 'generic-compose.html';
  this._enableEncryptionButton = null;
}

ComposeOverlayManager.prototype = Object.create(OverlayManagerBase.prototype);
ComposeOverlayManager.prototype.constructor = ComposeOverlayManager;

/**
 * Setup and begin operating.
 */
ComposeOverlayManager.prototype.setup = function () {

  OverlayManagerBase.prototype.setup.call(this);

  // Overlay a button to turn on encryption.
  this._enableEncryptionButton = document.createElement('div');
  this._enableEncryptionButton.classList.add('messageGuardIcon');
  this.overlayer.placeOverlay(this._enableEncryptionButton, Overlayer.OverlayPositions.TopRight);

  this._enableEncryptionButton.addEventListener('click', (function () {
    this.placeOverlay();
  }).bind(this));
};

/**
 * Called when the overlay is ready to communicate. Thus, messagehandlers should
 * be defined and registered here.
 */
ComposeOverlayManager.prototype.onOverlayReady = function () {

  var self = this;

  this.registerMessageHandler(MessageType.MESSAGE_ENCRYPTED, false, function (data) {
    if (self.item.matches('textarea,input')) {
      self.item.value = data;
    } else if (self.item.matches('[contentEditable]')) {
      self.item.innerHTML = data;
    } else {
      self.item.innerText = data;
    }
    self.removeOverlay();

    self.overlayer.placeOverlay(self._enableEncryptionButton, Overlayer.OverlayPositions.TopRight);
  });

  this.registerMessageHandler(MessageType.CANCEL, false, function (data) {
    if (self.item.matches('textarea,input')) {
      self.item.value = data;
    } else if (self.item.matches('[contentEditable]')) {
      self.item.innerHTML = data;
    } else {
      self.item.innerText = data;
    }
    self.removeOverlay();

    self.overlayer.placeOverlay(self._enableEncryptionButton, Overlayer.OverlayPositions.TopRight);
  });

  var data;
  if (this.item.matches('textarea,input')) {
    data = this.item.value;
  } else if (this.item.matches('[contentEditable]')) {
    data = this.item.innerHTML;
  } else {
    data = this.item.textContent;
  }

  //this.postMessage(MessageType.OVERLAY_READY, data);
};

module.exports = ComposeOverlayManager;
