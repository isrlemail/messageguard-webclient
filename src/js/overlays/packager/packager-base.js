/**
 * Abstract base overlay class.
 * @module
 */

"use strict";

var async = require('async');

var KeyManager = require('../../key-management/communicator/emitter'),
    encoder    = require('../../common/encoder'),
    Errors     = require('../../common/errors'),
    Identifier = require('../../key-management/key-prototypes').Identifier;

/**
 * @constructor
 * @alias module:frontend/overlay-base
 */
function PackagerBase() {

}

/**
 * Initialize
 */
PackagerBase.prototype.init = function () {
  throw new Errors.AbstractMethodError();
};

/**
 * Encrypts the given data
 * @param {string} data. The data to encrypt.
 */
PackagerBase.prototype.encrypt = function (data, keyAttributes, ids) {
  var fingerprint = keyAttributes.fingerprint;
  var scheme = keyAttributes.scheme;

  var self = this;

  var toEncrypt = JSON.stringify({
                                   data:   data,
                                   fromID: keyAttributes.id ? keyAttributes.id.serialize() : null
                                 });

  return this.encryptData(toEncrypt).then(function (encryptionPackage) {
    var encryptionKey = encryptionPackage.keyMaterial;
    var encryptedData = encryptionPackage.encrypted;

    // If the key we're encrypting with does not use recipients, just encrypt the data once
    // and all endpoints should be able to decrypt.
    if (!keyAttributes.canHaveRecipients) {
      ids = [new Identifier()];
    }

    return KeyManager.encryptMessageKey(encryptionKey, fingerprint, ids).then(function (encryptedKeys) {
      return self.stringifyEncryptedPackage(scheme, encryptedData, encryptedKeys);
    });
  });
};

/**
 * Encrypts the given data
 * @param {string} data. The data to encrypt.
 * @abstract
 */
PackagerBase.prototype.encryptData = function (data) {
  throw new Errors.AbstractMethodError();
};

/**
 * Decrypts the given data
 * @param {string} data. The data to decrypt.
 */
PackagerBase.prototype.decrypt = function (data) {
  var dataObj;

  try {
    dataObj = this.parseEncryptedPackage(data);
  } catch (e) {
    return Promise.reject(new Errors.PackagerError('Unable to parse encrypted package data.', e));
  }

  var self = this;

  return KeyManager.getKeyInfo().then(function (keyAttributesArray) {

    // Extract array of fingerprints.
    var availableFingerprints = keyAttributesArray.map(function (attributes) {
      return attributes.fingerprint;
    });

    // Filter encrypted keys by those for which we have a fingerprint.
    var candidateEncryptedKeys = dataObj.encryptedKeys.filter(function (encryptedKey) {
      return availableFingerprints.indexOf(encryptedKey.fingerprint) != -1;
    });

    // If there are no such encrypted keys, error out.
    if (candidateEncryptedKeys.length == 0) {
      throw new Errors.NoMatchingFingerprintsError('', {
        scheme:              dataObj.scheme,
        messageFingerprints: dataObj.encryptedKeys.map(function (key) {
          return key.fingerprint;
        })
      });
    }

    // Otherwise, try all of them in sequence until we succeed.
    return self.tryKeyDecrypt(candidateEncryptedKeys).then(function (result) {
      return self.decryptData({
                                keyMaterial: result.keyMaterial,
                                encrypted:   dataObj.encryptedData
                              }).then(function (decryptedData) {

        var decryptedData = JSON.parse(decryptedData);

        return {
          data:          decryptedData.data,
          fromID:        decryptedData.fromID ? Identifier.parse(decryptedData.fromID) : null,
          keyAttributes: result.keyAttributes
        };
      });
    });
  });
}

PackagerBase.prototype.stringifyEncryptedPackage = function (scheme, encryptedData, encryptedKeys) {
  var message = JSON.stringify({
                                 scheme:        scheme,
                                 encryptedData: encryptedData,
                                 encryptedKeys: encryptedKeys
                               });
  return '|mg|' + encoder.convert(message, 'utf8', 'base64');
};

PackagerBase.prototype.parseEncryptedPackage = function (stringified) {
  if (stringified.indexOf('|mg|', 0) !== 0) {
    throw new Errors.MalformedMessageError();
  }

  var parsed = JSON.parse(encoder.convert(stringified.substring('|mg|'.length), 'base64', 'utf8'));

  if ('scheme' in parsed && 'encryptedData' in parsed && 'encryptedKeys' in parsed) {
    return parsed;
  } else {
    throw new Errors.MalformedMessageError('Required properties not found');
  }
};

PackagerBase.prototype.tryKeyDecrypt = function (encryptedKeys) {
  return new Promise(function (resolve, reject) {
    var idx = -1;
    var tryNext = function (e) {

      if (e) {
        if (!(e instanceof Errors.KeyManagerError)) {
          return reject(e);
        }
      }

      if (++idx >= encryptedKeys.length) {
        return reject(new Errors.AllDecryptAttemptsFailedError());
      }

      KeyManager.decryptMessageKey(encryptedKeys[idx].encrypted, encryptedKeys[idx].fingerprint)
          .then(resolve)
          .catch(tryNext);
    };

    tryNext();
  });
};

/**
 * Decrypts the given data
 * @param {string} data. The data to encrypt.
 * @abstract
 */
PackagerBase.prototype.decryptData = function (data) {
  throw new Errors.AbstractMethodError();
};

/**
 * Checks on the packager
 * @abstract
 */
PackagerBase.prototype.check = function (data) {
  throw new Errors.AbstractMethodError();
};

module.exports = PackagerBase;
