/**
 * Methods to help examine the DOM.
 * @module
 */

// Polyfill the matches function where necessary.
// Polyfill for the window object.
if (!window.Element.prototype.matches) {
  window.Element.prototype.matches =
      window.Element.prototype.webkitMatchesSelector || window.Element.prototype.mozMatchesSelector ||
      window.Element.prototype.msMatchesSelector || window.Element.prototype.oMatchesSelector ||
      function (selector) {
        var matches = (this.document || this.ownerDocument).querySelectorAll(selector);
        for (var i = 0, len = matches.length; i < len; i++) {
          if (this === matches[i]) {
            return true;
          }
        }
        return false;
      };
}

/**
 * Functions for selecting elements.
 * @type {Object}
 */
module.exports = {

  /**
   * Determines if ancestor is node's actual ancestor.
   * @param {Node!} node Node to test.
   * @param {Node!} ancestor Potential ancestor.
   * @returns {boolean}
   */
  isAncestor: function (node, ancestor) {
    while ((node = node.parentNode) !== null) {
      if (node === ancestor) return true;
    }
    return false;
  },

  /**
   * Find the first ancestor that matches the given selector.
   * @param {Node!} node Node to test.
   * @param {string} selector CSS Selector to test ancestors against.
   * @returns {Node} Ancestor that matches selector or null if none do.
   */
  findAncestor: function (node, selector) {
    while ((node = node.parentNode) !== null) {
      if (node.matches(selector)) return node;
    }
    return null;
  }

};