/**
 * This makes bootstrap work more like the Node.JS way of doing things.
 * We initialize jQuery and then re-export our new version. Use this require if you want bootstrap support. Keeps
 * the global space clean.
 */

// Only require this code if we're not in a worker thread. Downstream users of jQuery
// will have to check if it's available before they use it.

var isWorker = typeof window === 'undefined';

if (isWorker) {
  // Usually this happens when a key system requires jQuery for UI purposes.
  // This is fine, since in a worker thread the UI functions are not called.
  // Still, best to warn key system developers in case they attempt to use
  // jQuery functions outside of a UI method.
} else {
  // Load jQuery into the global namespace.
  var jQuery = require('jquery');
  global.jQuery = jQuery;

  // Add Tether functionality to Bootstrap.
  // Must come before `require('bootstrap')`
  global.Tether = require('tether');

  // Add bootstrap functionality to jQuery.
  require('bootstrap');

  // Remove jQuery from the global namespace.
  jQuery = global.jQuery;
  global.jQuery = undefined;

  // Export our new jQuery module.
  module.exports = jQuery;
}
