var BaseStorage  = require('./base-storage'),
    Errors       = require('../errors'),
    EventEmitter = require('./storage-objects').eventEmitter,
    UUID         = require('../uuid');


/**
 *
 * Central storage module for key management.
 *
 * Some key systems should be placed in session storage; we do not want them to persist
 * long-term. However, sessionStorage is not shared between windows. Since multiple
 * windows are used to enter key data on encryption or decryption, this is unsuitable.
 * This module allows us to coordinate and sync "session-like" storage that is purged
 * when the last instance is killed.
 *
 * To address this issue, there is a 'sessionStorageData' object kept in memory.
 * Whenever any instance writes to that object, an update is broadcast, and every instance
 * receives the update and applies it to their copy of the object. Since it is in-memory only,
 * when the last instance is destroyed, the session state is destroyed as well.
 *
 * When an instance starts up, it requests the session storage state from all other instances.
 * Only the first reply is efficacious, since we're using the EventEmitter.once method.
 */

function SyncedMemoryStorage() {
  BaseStorage.call(this, 'synced-memory');

  /**
   * @type {string} - used to coordinate and respond to heartbeats, as well as to prevent the same
   *                  instance from responding to its own 'storage-changed' event.
   */
  this.instanceUUID = UUID();

  /**
   * Object for containing storage data.
   */
  this.sessionStorageData = {};
};

SyncedMemoryStorage.prototype = Object.create(BaseStorage.prototype);
SyncedMemoryStorage.constructor = SyncedMemoryStorage;

SyncedMemoryStorage.prototype._getRawKeys = function () {
  return Promise.resolve(Object.keys(this.sessionStorageData));
};

/**
 * Initializes storage event listeners.
 */
SyncedMemoryStorage.prototype._initialize = function () {
  var self = this;

  /**
   * Listens for storage changes. Applies any updates it receives.
   */

  EventEmitter.on('SESSION_STORAGE_UPDATE', function (e) {
    if (e.details.instanceUUID == self.instanceUUID) {
      return;
    }

    switch (e.details.eventType) {
      case 'set':
        self.sessionStorageData[e.details.key] = e.details.valueString;
        break;
      case 'remove':
        delete self.sessionStorageData[e.details.key];
        break;
    }
  });

  /**
   * Listens for session storage requests. Responds with the contents of session storage.
   */
  EventEmitter.on('SESSION_STORAGE_REQUEST', function (e) {
    if (e.details.requesterUUID == self.instanceUUID) {
      return;
    }

    EventEmitter.emit(e.details.requesterUUID + '_SESSION_STORAGE_RESPONSE', self.sessionStorageData);
  });

  /**
   * Listens for storage responses. Only called once, since multiple instances might be responding.
   */
  EventEmitter.once(this.instanceUUID + '_SESSION_STORAGE_RESPONSE', function (e) {
    for (var key in e.details) {
      self.sessionStorageData[key] = e.details[key];
    }
  });

  EventEmitter.emit('SESSION_STORAGE_REQUEST', {
    requesterUUID: this.instanceUUID
  });

  return Promise.resolve();
};

/**
 * Issues a storage update event.
 *
 * @param {string} eventType - {set, remove}
 * @param {string} key
 * @param {string} valueString - undefined if eventType == 'remove'
 */
SyncedMemoryStorage.prototype.triggerSessionUpdate = function (eventType, key, valueString) {
  EventEmitter.emit('SESSION_STORAGE_UPDATE', {
    instanceUUID: this.instanceUUID,
    eventType:    eventType,
    key:          key,
    valueString:  valueString
  });
}

/**
 * Retrieves an item from storage.
 * Undoes the JSON-encoding that was performed during setItem.
 *
 * @param {string} key
 * @returns {*}
 */
SyncedMemoryStorage.prototype._getItem = function (key) {
  if (!(key in this.sessionStorageData)) {
    return Promise.resolve(null);
  }

  try {
    return Promise.resolve(JSON.parse(this.sessionStorageData[key]).v);
  } catch (e) {
    return Promise.resolve(null);
  }
};

/**
 * Stores an item in storage. Performs JSON-encoding.
 *
 * @param {string} key
 * @param {*} value
 */
SyncedMemoryStorage.prototype._setItem = function (key, value) {
  var valueString = JSON.stringify({v: value});

  this.sessionStorageData[key] = valueString;
  this.triggerSessionUpdate('set', key, valueString);
  return Promise.resolve();
};

/**
 * Removes an item from storage.
 *
 * @param {string} key
 */
SyncedMemoryStorage.prototype._removeItem = function (key) {
  delete this.sessionStorageData[key];
  this.triggerSessionUpdate('delete', key);
  return Promise.resolve();
};

module.exports = new SyncedMemoryStorage();