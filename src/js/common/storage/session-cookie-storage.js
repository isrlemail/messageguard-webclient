var BaseStorage    = require('./base-storage'),
    encoder        = require('../encoder'),
    Encryptor      = require('../encryptor'),
    Errors         = require('../errors'),
    StorageObjects = require('./storage-objects');

var COOKIE_NAME = 'storage-encryption-key';

function SessionCookieStorage() {
  BaseStorage.call(this, 'managed-session');

  var self = this;

  this.readyResolve = null;
  this.ready = new Promise(function (resolve) {
    self.readyResolve = resolve;
  });
}

SessionCookieStorage.prototype = Object.create(BaseStorage.prototype);
SessionCookieStorage.constructor = SessionCookieStorage;

SessionCookieStorage.prototype._getRawKeys = function () {
  return StorageObjects.local.getKeys();
};

SessionCookieStorage.prototype._initialize = function () {
  var self = this;

  return this._getSessionCookie().then(function (encryptionKey) {
    self.encryptionKey = encryptionKey;

    if (!Encryptor.isKey(self.encryptionKey)) {
      self.encryptionKey = Encryptor.randomKey();
      return self._setSessionCookie(self.encryptionKey)
    }
  }).then(this.readyResolve);
};

SessionCookieStorage.prototype._getItem = function (key) {
  var self = this;

  return this.ready.then(function () {
    return StorageObjects.local.getItem(key);
  }).then(function (encryptedData) {
    if (!encryptedData) {
      return null;
    }

    encryptedData = JSON.parse(encryptedData);

    try {
      return self._decryptData(encryptedData);
    } catch (e) {
      return self._removeItem(key).then(function () {
        return null;
      });
    }
  });
};

SessionCookieStorage.prototype._setItem = function (key, value) {
  var self = this;

  return this.ready.then(function () {
    var encryptedData = self._encryptData(value),
        encryptedData = JSON.stringify(encryptedData);

    return StorageObjects.local.setItem(key, encryptedData);
  });
};

SessionCookieStorage.prototype._removeItem = function (key) {
  return this.ready.then(function () {
    return StorageObjects.local.removeItem(key);
  });
};

SessionCookieStorage.prototype._encryptData = function (data) {
  data = JSON.stringify(data);
  return Encryptor.encrypt(data, this.encryptionKey);
};

SessionCookieStorage.prototype._decryptData = function (encryptedData) {
  var data;

  try {
    var data = Encryptor.decrypt(encryptedData, this.encryptionKey);
  } catch (e) {
    throw new Errors.StorageError('Encrypted storage signature invalid');
  }

  try {
    return JSON.parse(data);
  } catch (e) {
    throw new Errors.StorageError('Could not parse decrypted storage data.');
  }
};

SessionCookieStorage.prototype._setSessionCookie = function (value) {
  return StorageObjects.cookies.setCookie(COOKIE_NAME, value, '/');
};

SessionCookieStorage.prototype._getSessionCookie = function () {
  return StorageObjects.cookies.getCookie(COOKIE_NAME);
};

module.exports = new SessionCookieStorage();
