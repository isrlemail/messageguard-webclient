/**
 * Key management initialization file.
 */

var $          = require('../../common/jquery-bootstrap'),
    KeyManager = require('../key-manager'),
    KeySchemes = require('../key-schemes'),
    KeyStorage = require('../key-storage'),
    urls       = require('../../common/urls');

/**
 * Shows the welcome message. Returns a promise that resolves as soon as the modal is dismissed.
 */
function showStartupModal() {
  return new Promise(function (resolve) {
    $(function () {
      $('#startup-modal').modal('show');
      $('#startup-modal .next-btn').click(function () {
        $('#startup-modal').modal('hide');
        resolve();
      });
    });
  });
}

/**
 * When initialization is complete, attempt to dispatch an 'initialization_complete'
 * message to the opener's kango object. This will let the extension backend know that
 * initialization is complete.
 */

var isCompleted = false;

function onComplete() {
  isCompleted = true;
  $('.close-btn').show();
  if (urls.isExtension && window.opener && window.opener.kango) {
    window.opener.kango.dispatchMessage('initialization_complete');
  }
}

$('.close-btn').hide().click(function () {
  window.close();
});

window.addEventListener('beforeunload', function (e) {
  if (!isCompleted) {
    var confirmationMessage = 'MessageGuard has not completed initialization. Please continue before closing.';
    e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
    return confirmationMessage;              // Gecko, WebKit, Chrome <34
  }
});

if (!KeyStorage.needsMasterPassword()) {
  // We'll be skipping the master password modal.
  $('.initialization-message').hide();

  if (KeySchemes.hasSchemeWithInitFunction()) {
    $('.no-master-password-message').show();
  } else {
    // We'll also be skipping initialization.
    $('.nothing-to-do-message').show();

    // No 'next' button to click.
    $('#startup-modal .modal-footer .next-btn').hide();
    onComplete();
  }
}


// Show the welcome message. Then, if necessary, prompt for a master password.
// Then intialize keys. Finally, show the complete modal and notify the extension
// backend that initialization has completed.

showStartupModal().then(function () {
  return KeyManager.ensureKeyStoragePasswordPresent();
}).catch(function (e) {
  return KeyManager.promptMasterPassword(e);
}).then(function () {

  return new Promise(function (resolve) {
    var tryInitialize = function () {
      $('#key-initialization-modal .error-message').hide();
      $('#key-initialization-modal .loading-message').show();
      return KeyManager.initializeKeys().then(resolve);
    };

    var tryCatch = function () {
      tryInitialize().catch(function (error) {
        $('#key-initialization-modal .loading-message').hide();
        $('#key-initialization-modal .error-message').show().find('.alert-text').html(error.message);
      });
    };

    $('#key-initialization-modal .error-message .retry-btn').click(tryCatch);

    $('#key-initialization-modal').modal('show');
    tryCatch();
  });
}).then(function () {
  $('#key-initialization-modal').modal('hide');
  $('#initialization-complete-modal').modal('show');

  onComplete();
});
