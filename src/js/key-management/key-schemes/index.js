var Errors = require('../../common/errors');

// Map of scheme names to KeyScheme instances
var schemes = {};

/**
 * Adds a scheme to the 'schemes' map. Throws an exception if there are name collisions.
 * @param scheme - instance of KeyScheme to add
 */
var addScheme = function (scheme) {
  if (scheme.name in schemes) {
    throw new Errors.KeyManagerError('Duplicated scheme name: ' + scheme.name);
  }

  schemes[scheme.name] = scheme;
};

// Add schemes here
// Normally we'd do something like 'requireDir()', but browserify
// doesn't play nice with that package.

//addScheme(require('./passwords/persistent'));
addScheme(require('./passwords/ephemeral'));
//addScheme(require('./ibe'));
//addScheme(require('./pgp'));

module.exports = {
  getNames: function () {
    return Object.keys(schemes);
  },

  getScheme: function (schemeName) {
    return schemes[schemeName];
  },

  hasSchemeWithEncryptedStorage: function () {
    return this.getNames().some(function (schemeName) {
      return schemes[schemeName].usesEncryptedStorage;
    });
  },

  hasSchemeWithInitFunction: function () {
    return this.getNames().some(function (schemeName) {
      return typeof schemes[schemeName].init === 'function';
    });
  }
};
