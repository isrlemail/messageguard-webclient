/*
* Shared symmetric key that encrypts via AES
*/

var PGP  = require('../../../../lib/openpgp/openpgp'),
    util = require('util');

var KeyPrototypes = require('../key-prototypes'),
    $             = require('../../common/jquery-bootstrap'),
    Ajax          = require('../../common/ajax'),
    Errors        = require('../../common/errors'),
    encoder       = require('../../common/encoder'),
    Encryptor     = require('../../common/encryptor');

Errors.registerErrorType(Errors.KeyManagerError, 'PGPError');
Errors.registerErrorType(Errors.PGPError, 'PublicKeyNotFoundError');

// ******************************
//
// Key Scheme
//
// ******************************

var SCHEME_NAME = 'PGP';
var KEYSERVER_HOSTNAME = 'keys.messageguard.io';

var PGPScheme = function () {
  this.name = SCHEME_NAME;
};

PGPScheme.prototype = new KeyPrototypes.KeySchemeBase();

/**
 * Called to initialize PGP keys
 *
 * Gets a list of identities currently known to the key server, and
 * automatically creates keys for each identity.
 *
 * @promise {PGPSystem[]} - list of new PGP systems created
 * @reject error - AJAX error if encountered
 */
PGPScheme.prototype.init = function (callbacks) {
  var errorMessage = 'Error initializing PGP keys. Please make sure you are signed in to an account at <a href="https://' +
                     KEYSERVER_HOSTNAME + '" target="_blank">MessageGuard.io</a> and have validated an identity.';

  return callbacks.getAllSchemeAttributes().then(function (existingAttributes) {
    if (existingAttributes.length) {
      // Don't bother fetching an ID list from the keyserver if we already have keys.
      return;
    }

    return new Promise(function (resolve, reject) {
      Ajax.post(
          {
            url:             'https://' + KEYSERVER_HOSTNAME + '/api/whoami',
            withCredentials: true,
            responseType:    'json',
            
            success:         function (data) {
              if (!data.identities) {
                return reject(new Errors.KeyInitializationError(errorMessage));
              }

              Promise.all(data.identities.map(function (identity) {
                identity = new KeyPrototypes.Identifier(identity.value, identity.type);

                return PGPSystem._generateSystem(identity).then(function (newKey) {
                  newKey.attributes.name = util.format('%s key (%s)', SCHEME_NAME, identity.value);

                  return callbacks.addKey(newKey);
                });
              })).then(resolve);
            },

            error:           function (statusCode, response) {
              reject(new Errors.KeyInitializationError(errorMessage));
            }
          });
    });
  });
};

/**
 * Given a PGPSystem and callback for setting save-state, produce
 * an element configured for editing the key system.
 *
 * @param {PGPSystem} keySystem - system for editing. Might be null, in which case this is a new system editor.
 * @callback callbacks - object with callbacks to interact with UI manager.
 * @promise {JQuery} - element for editing the key system.
 */
PGPScheme.prototype.getUI = function (keySystem, callbacks) {
  var self = this;

  var editor = $('<div> \
  					<div class="input-group"> \
  					  <select class="pgp-id-type form-control" style="width:20%"> \
						  <option value="Email" selected="selected">Email</option> \
						  <option value="Facebook">Facebook</option> \
						  <option value="MessageGuard" disabled>MessageGuard</option> \
						</select> \
					  <input type="text" class="pgp-id form-control" placeholder="Enter your email address" style="width:80%"> \
					  <span class="input-group-btn"> \
						<button class="verify-btn btn btn-default" type="button" data-default-text="Verify" data-success-text="Verified" data-error-text="Error"></button> \
					  </span> \
					</div> \
  				<div class="alert alert-danger" role="alert"> \
  					<div class="row"> \
  						<div class="col-md-10">\
					  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> \
					  <span class="alert-text"></span> \
					</div> \
					<br /><br />\
					<div class="col-md-2"> \
					<button type="button" class="retry-btn btn btn-info">Retry</button> \
					</div> \
					</div> \
					</div> \
				</div>');

  var idTypeSelect = editor.find('.pgp-id-type');
  var idField = editor.find('.pgp-id');
  var verifyBtn = editor.find('.verify-btn');
  var alertDiv = editor.find('.alert');
  var retryBtn = alertDiv.find('.retry-btn');

  /**
   * Configures the verify-btn element's text value.
   *
   * @param {string} status - status name to set text for.
   */
  var setVerifyBtnText = function (status) {
    var attr_name = 'data-' + status + '-text';

    verifyBtn.text(verifyBtn.attr(attr_name));
  };

  /**
   * Shows an alert on error.
   *
   * @param {string} alert - HTML representation of an alert value.
   * @callback tryAgainCB - function to be called when the user wants to try the action again.
   */
  var showAlert = function (alert, tryAgainCB) {
    if (alert) {
      alertDiv.find('.alert-text').html(alert);
      retryBtn.prop('disabled', false).toggle(typeof tryAgainCB != 'undefined')
          .off('click').click(function () {
        $(this).prop('disabled', true);
        tryAgainCB();
      });

      alertDiv.show();
    } else {
      alertDiv.hide();
    }
  };

  idTypeSelect.val(keySystem ? keySystem.attributes.id.type : idTypeSelect.find('option[selected]').val());
  idField.val(keySystem ? keySystem.attributes.id.value : '');
  setVerifyBtnText(keySystem ? 'success' : 'default');
  verifyBtn.prop('disabled', true);
  callbacks.isSaveable(keySystem ? true : false);
  showAlert(null);

  var verifiedID = new KeyPrototypes.Identifier();

  /**
   * Checks whether the ID has remained unchanged from the last-verified ID.
   *
   * @returns {boolean}
   */
  var idUnchanged = function () {
    return idTypeSelect.val() === verifiedID.type && idField.val() === verifiedID.value;
  };

  idTypeSelect.add(idField).on('input change propertychange paste', function () {
    setVerifyBtnText('default');
    callbacks.isSaveable(idUnchanged());
    verifyBtn.prop('disabled', idUnchanged());
  });

  /**
   * Function to verify ownership of the entered ID.
   * If successful, enables saving.
   * If unsuccessful, displays an appropriate error.
   */
  var verifyID = function () {
    verifyBtn.prop('disabled', true);

    var id = new KeyPrototypes.Identifier(idField.val(), idTypeSelect.val());

    self._verifyID(id).then(function () {
      callbacks.isSaveable(true);
      showAlert(null);
      setVerifyBtnText('success');

      verifiedID.type = id.type;
      verifiedID.value = id.value;
    }).catch(function (err) {
      callbacks.isSaveable(false);
      setVerifyBtnText('default');

      var errText = {
        _401:     'Your identity needs to be verified. Visit <a target="_blank" href="https://' + KEYSERVER_HOSTNAME +
                  '[LOGIN_URL]">' + KEYSERVER_HOSTNAME + '</a> to log in and verify your identity.',
        _404:     'Your identity needs to be registered. Visit <a target="_blank" href="https://' + KEYSERVER_HOSTNAME +
                  '">' + KEYSERVER_HOSTNAME + '</a> to register an account and verify your identity.',
        _default: 'Unknown error occurred. Visit <a target="_blank" href="https://' + KEYSERVER_HOSTNAME + '">' +
                  KEYSERVER_HOSTNAME + '</a> to resolve.'
      };

      switch (err.details.code) {
        case 404:
          showAlert(errText._404, verifyID);
          break;

        case 401:
          showAlert(errText._401.replace('[LOGIN_URL]', err.details.text), verifyID);
          break;

        default:
          showAlert(errText._default, verifyID);
      }
    });
  };

  verifyBtn.click(verifyID);

  return Promise.resolve(editor);
};

/**
 * Verifies a given identity against the key server to ensure the
 * key server will grant access to the identity's private key.
 *
 * If verification is successful, the private key and public parameters will be returned.
 *
 * @param {Identifier} id
 * @promise {Object.<string, number[]>} - object containing the private key and public parameters.
 * @reject {Object.<string, string>} - error object
 * @private
 */
PGPScheme.prototype._verifyID = function (id) {
  return new Promise(function (resolve, reject) {
    var errMessage = 'Failed to validate ownership of identity: ' + id.serialize();

    // TODO - fix this so it works with the routine above to prove ownership, while
    //        returning meaningful error codes.

    Ajax.post({
                url:             'https://' + KEYSERVER_HOSTNAME + '/api/whoami',
                withCredentials: true,
                responseType:    'json',
                success:         function (r) {
                  if (!r.identities) {
                    return reject(new Errors.PGPError(errMessage, {code: 404}));
                  }

                  for (var i = 0; i < r.identities.length; i++) {
                    if (id.value == r.identities[i].value && id.type == r.identities[i].type) {
                      return resolve();
                    }
                  }

                  reject(new Errors.PGPError('Unable to verify ID', {code: 404}));
                },
                error:           function (code, response) {
                  reject(new Errors.PGPError(errMessage, e));
                }
              });
  });
};

/**
 * Retrieves a public key from the key server. Performs client-side caching.
 * Caching is not persistent, and will be cleared when the PGPSystem object is destroyed.
 *
 * @param {Identifier} id - object representing the identity to get a key for.
 * @promise {Object.<string, string>} - PGP public key for the given ID, along with its fingerprint.
 * @reject {*} - rejects if identity does not exist.
 * @private
 */
PGPScheme._getPublicKey = function (id) {

  this.publicKeyCache = this.publicKeyCache || {};

  var idString = JSON.stringify({
                                  identity: {type: id.type, value: id.value}
                                });

  if (idString in this.publicKeyCache) {
    return Promise.resolve(this.publicKeyCache[idString]);
  }

  var self = this;

  return new Promise(function (resolve, reject) {
    Ajax.post({
                url:          'https://' + KEYSERVER_HOSTNAME + '/api/public/pgp',
                responseType: 'text',
                contentType:  'application/json',
                data:         idString,
                success:      function (idKey) {
                  var result = {
                    publicKey:   PGP.key.readArmored(idKey),
                    fingerprint: PGPSystem._getFingerprint(idKey)
                  };

                  self.publicKeyCache[idString] = result;

                  resolve(result);
                },
                error:        function (code) {
                  if (code == 404) {
                    reject(
                        new Errors.PublicKeyNotFoundError('Identity not found on PGP key server: ' + id.serialize()));
                  } else {
                    reject(new Errors.PGPError('Unable to get public key for identity: ' + id.serialize()));
                  }
                }
              });
  });
};

PGPScheme.prototype.handleError = function (keyError, editor, resolved) {
  // TODO
};

/**
 * Given an element for an editor, create a new PGPSystem instance.
 *
 * @param {JQuery} editor
 */
PGPScheme.prototype.create = function (editor, fingerprintsToMatch) {
  var id_value = $(editor).find('.pgp-id').val();
  var id_type = $(editor).find('.pgp-id-type').val();

  var id = new KeyPrototypes.Identifier(id_value, id_type);

  // If we've gotten to this point, we can't generate a PGP key from a public fingerprint,
  // so just error out.
  if (fingerprintsToMatch && fingerprintsToMatch.length) {
    return Promise.reject(new Errors.KeyCreateFailedError('Cannot create a PGP key to match an ID.'));
  }

  return PGPSystem._generateSystem(id);
};

/**
 * Given an element for an editor and an existing PGPSystem instance, update the instance.
 *
 * @param {PGPSystem} keySystem - system to update.
 * @param {JQuery} editor - element representing the new key.
 */
PGPScheme.prototype.update = function (keySystem, editor) {
  var id = $(editor).find('.pgp-id').val();
  var id_type = $(editor).find('.pgp-id-type').val();

  var id = new KeyPrototypes.Identifier(id_value, id_type);

  return PGPSystem._updateSystem(keySystem, id);
};

/**
 * Serializes a system into a string representation.
 *
 * @param {PGPSystem} keySystem - system to serialize.
 * @promise {string} - serialized representation.
 */
PGPScheme.prototype.serialize = function (system) {
  var serialized = JSON.stringify({
                                    publicKey:  system.publicKey,
                                    privateKey: system.privateKey
                                  });

  return Promise.resolve(serialized);
};

/**
 * Parses a PGPSystem instance from a serialized representation.
 *
 * @param {string} serializedSystem - serialized representation of a key system.
 * @promise {PGPSystem} - parsed system
 */
PGPScheme.prototype.parse = function (serializedSystem) {
  var parsed = JSON.parse(serializedSystem);

  var publicKey = parsed.publicKey;
  var privateKey = parsed.privateKey;

  return Promise.resolve(new PGPSystem(publicKey, privateKey));
};


// ******************************
//
// Key System
//
// ******************************

/**
 * Constructs a new PGPSystem instance.
 *
 * @param {Identifier} id - identifier for key
 * @param {string} publicKey - public key
 * @param {string} privateKey - private key
 * @constructor
 */
var PGPSystem = function (publicKey, privateKey) {
  this.attributes = new KeyPrototypes.KeyAttributes();
  this.attributes.scheme = SCHEME_NAME;
  this.attributes.canSelect = true;
  this.attributes.canHaveRecipients = true;

  this.publicKey = publicKey;
  this.privateKey = privateKey;

  this._updateFingerprint();
};

/**
 * Generates a system for the given id and type.
 * Uploads the public key after key creation.
 *
 * @param {Identifier} id - id for key
 * @promise {PGPSystem} - new PGP key created
 */
PGPSystem._generateSystem = function (id) {
  return new Promise(function (resolve) {
    PGPSystem._generateKey(id).then(function (keys) {

      var done = function () {
        var system = new PGPSystem(keys.publicKey, keys.privateKey);
        system.attributes.id = id;
        resolve(system);
      };

      PGPSystem._putPublicKey(id, keys.publicKey)
          .then(done)
          .catch(function (e) {
            done();
          });
    });
  });
};

/**
 * Updates a system for a new id and type.
 *
 * Generates a new system, then copies the critical values over and updates the fingerprint.
 *
 * @param {PGPSystem} system - system to update
 * @param {Identifier} id - id for new key
 */
PGPSystem._updateSystem = function (system, id) {
  return PGPSystem._generateSystem(id).then(function (newSystem) {
    system.attributes.id = newSystem.attributes.id;
    system.publicKey = newSystem.publicKey;
    system.privateKey = newSystem.privateKey;
    system._updateFingerprint();
  });
};

/**
 * Generates a random PGP key for a give id and type.
 *
 * @param {Identifier} id - id for new key.
 * @promise {Object.<string, string>} public and private keys.
 */
PGPSystem._generateKey = function (id) {
  var options = {
    numBits: 2048,
    userIds: [{name: id.type + ':' + id.value}]
  };

  return PGP.generateKey(options).then(function (keyPair) {
    var publicKey = keyPair.publicKeyArmored;
    var privateKey = keyPair.privateKeyArmored;

    return {
      publicKey:  publicKey,
      privateKey: privateKey
    };
  });
};

PGPSystem.prototype = new KeyPrototypes.KeySystemBase();

/**
 * Updates a key's fingerprint.
 * @private
 */
PGPSystem.prototype._updateFingerprint = function () {
  this.attributes.fingerprint = PGPSystem._getFingerprint(this.publicKey);
};

/**
 * Gets the fingerprint for a given public key.
 *
 * Performs SHA hashing
 *
 * @param {string} publicKey - key to generate fignerprint for.
 * @private
 */
PGPSystem._getFingerprint = function (publicKey) {
  return Encryptor.hash(publicKey);
};

/**
 * Uploads a public key to the key server.
 *
 * @param {Identifier} id - id for key.
 * @param {string} publicKey - public key to upload.
 * @promise {*} - resolves when upload complete.
 * @reject {*} - fails when not authorized.
 * @private
 */
PGPSystem._putPublicKey = function (id, publicKey) {
  var dataString = JSON.stringify({
                                    contents: publicKey,
                                    identity: {
                                      type:  id.type,
                                      value: id.value
                                    }
                                  });

  return new Promise(function (resolve, reject) {
    Ajax.put({
               url:             'https://' + KEYSERVER_HOSTNAME + '/api/public/pgp',
               contentType:     'application/json',
               data:            dataString,
               withCredentials: true,
               success:         resolve,
               error:           function (code, response) {
                 reject(new Errors.PGPError('Unable to upload public key to keyserver (' + code + ').', response));
               }
             });
  });
};

/**
 * Checks whether this key system can encrypt for the given ID.
 * If a public key exists for this user, then encryption can proceed.
 *
 * @param {Identifier} id - id to check
 * @promise {Object.<string, *>} - whether this key can encrypt for the given ID.
 */
PGPSystem.prototype.canEncrypt = function (id) {
  return new Promise(function (resolve) {
    PGPScheme._getPublicKey(id).then(function () {
      resolve({
                id:         id,
                canEncrypt: true,
                message:    ''
              });
    }).catch(function (err) {
      // TODO - just throw an error?
      resolve({
                id:         id,
                canEncrypt: false//,
                //message:    PROMPT_INSTALL_MSG.replace(/DEST_EMAIL_ADDRESS/g, encodeURIComponent(id.value))
              });
    });
  });
};

/**
 * Performs PGP encryption on a given piece of data.
 *
 * @param {string} data - data to encrypt
 * @param {Identifier} id - id to encrypt for
 * @promise {Object.<string, string>} - result of encryption, including the fingerprint for the receiving side.
 */
PGPSystem.prototype.encrypt = function (data, id) {
  return PGPScheme._getPublicKey(id).then(function (key) {
    return PGP.encrypt({
                         data:       data,
                         publicKeys: key.publicKey.keys,
                         armor:      true
                       })
        .then(function (result) {
          return {
            encrypted:   result.data,
            fingerprint: key.fingerprint
          };
        });
  }).catch(function (e) {
    if (e instanceof Errors.PublicKeyNotFoundError) {
      throw new Errors.RecipientMustInstallError('Recipient ' + id.value +
                                                 ' must install and set up MessageGuard first.', {destinationAddress: id.value});
    } else {
      throw new Errors.EncryptFailedError('Error encrypting for ID: ' + id.serialize() + ', ' + e.toString(), e);
    }
  });
};

/**
 * Performs PGP decryption.
 *
 * @param {string} data - data to be decrypted
 * @promise {string} - result of decryption
 */
PGPSystem.prototype.decrypt = function (data) {
  var privKeys = PGP.key.readArmored(this.privateKey);
  var privKey = privKeys.keys[0];

  var message = PGP.message.readArmored(data);

  return PGP.decrypt({
                       message:    message,
                       privateKey: privKey
                     }).then(function (result) {
    return result.data;
  });
};

/**
 * Performs PGP signing on a piece of data.
 *
 * @param {string} data - data to sign.
 * @promise {string} - signature data.
 */
PGPSystem.prototype.sign = function (data) {
  var priv = PGP.key.readArmored(this.privateKey);
  var privKey = priv.keys[0];

  var self = this;

  return PGP.sign({
                    data:        data,
                    privateKeys: privKey,
                    armor:       true
                  }).then(function (result) {
    return JSON.stringify({
                            message:   result.data,
                            signer_id: self.attributes.id.serialize()
                          });
  });
};

/**
 * Performs PGP verification on a signature.
 *
 * @param {string} data - original data that was signed. In this implementation, unnecessary.
 * @param {string} signature - signature data for verification.
 * @promise {boolean} - whether or not the signature is valid.
 */
PGPSystem.prototype.verify = function (data, signature) {
  var signature = JSON.parse(signature);
  var keyServerUrl = 'https://' + KEYSERVER_HOSTNAME + '/api/public/pgp';

  var signer = KeyPrototypes.Identifier.parse(signature.signer_id);

  return PGPScheme._getPublicKey(signer).then(function (result) {
    var cleartextMessage = PGP.cleartext.readArmored(signature.message);

    return new Promise(function (resolve) {
      PGP.verify({
                   message:    cleartextMessage,
                   publicKeys: result.publicKey.keys
                 }).then(function (result) {
        if (!result.signatures || !result.signatures.length) {
          resolve(false);
          return;
        }

        for (var i = 0; i < result.signatures.length; i++) {
          if (!result.signatures[i].valid) {
            resolve(false);
            return;
          }
        }

        resolve(true);
      });
    });
  });
};

module.exports = new PGPScheme();
