var $            = require('../common/jquery-bootstrap'),
    Encryptor    = require('../common/encryptor'),
    Errors       = require('../common/errors'),
    KeyUIManager = require('./key-ui-manager'),
    KeyStorage   = require('./key-storage'),
    KeySchemes   = require('./key-schemes');

/**
 * Constructor. Loads up the uiManagers, and initializes all key schemes.
 *
 * KeyScheme.prototype.init is called for each scheme for which we have no keys in storage.
 * Any keys created are added to storage.
 *
 * @constructor
 */
function KeyManager() {
  this.uiManagers = KeySchemes.getNames().reduce(function (managerMap, name) {
    managerMap[name] = new KeyUIManager(KeySchemes.getScheme(name));
    return managerMap;
  }, {});
}

/**
 * Performs key initialization for each of the schemes.
 *
 * @returns Promise - resolved when all keys are initialized.
 */
KeyManager.prototype.initializeKeys = function () {
  return Promise.all(KeySchemes.getNames().map(function (schemeName) {
    if (typeof KeySchemes.getScheme(schemeName).init === 'function') {

      var callbacks = {
        getAllSchemeAttributes: KeyStorage.getAttributesByScheme.bind(KeyStorage, schemeName),
        addKey:                 KeyStorage.add.bind(KeyStorage)
      };

      return KeySchemes.getScheme(schemeName).init(callbacks);
    }
  }));
};

/**
 * Creates the "Add new key" button UI element for a scheme.
 *
 * @param uiManager
 * @returns {*|jQuery|HTMLElement}
 */
KeyManager.prototype.setupUI = function () {

  var _makeAddButton = function (uiManager) {
    var button = $('<button class="add-key-button fa fa-plus"></button>');
    button.click(function () {
      uiManager.addKeySystem('standard');
    });
    return button;
  };

  /**
   * Sets up the tab for the given key scheme.
   *
   * @param {KeyUIManager} uiManager
   * @param {number} index of tab, used for ids.
   */
  var setupTab = function (uiManager, index) {
    var systemTypeTab = $('<li class="key-system-tab" role="presentation"><a role="tab" data-toggle="tab"></a></li>');
    systemTypeTab.find('a').attr('href', '#key-scheme-' + index).text(uiManager.scheme.name);

    var systemTypeSection = $(
        '<div id="key-scheme-' + index + '" class="key-system-section tab-pane fade in"></div>');

    systemTypeSection.append(_makeAddButton(uiManager));
    systemTypeSection.append($('<h1></h1>').text('Add new key (' + uiManager.scheme.name + ')'));

    return uiManager.setupUI().then(function (ui) {
      systemTypeSection.append(ui);

      systemTypeTab.appendTo('#manager-tabs');
      systemTypeSection.appendTo('#manager-tab-content');
    });
  };

  return Promise.all(Object.keys(this.uiManagers).map(function (manager, i) {
    var uiManager = this.uiManagers[manager];
    return setupTab(uiManager, i);
  }, this)).then(function () {
    $('#manager-tabs .key-system-tab:eq(0)').addClass('active');
    $('#manager-tab-content .key-system-section:eq(0)').addClass('active');
  });
};

/**
 * Prompts the user to create key matching the given fingerprint.
 *
 * @param {string} schemeName - name of scheme to create key for
 * @param {string} promptType - 'decrypt', 'encrypt', or 'standard'
 * @param {boolean} fullKeyOptions - whether to show the name and color chooser.
 * @param {string} fingerprint - if supplied, created key must match.
 * @promise {KeySystem} - key system created.
 * @reject {Object<string, string>} - error message if scheme was not found.
 */
KeyManager.prototype.createKey = function (schemeName, promptType, fullKeyOptions, fingerprints) {

  var uiManager = this.uiManagers[schemeName];

  if (uiManager) {
    return uiManager.addKeySystem(promptType, fullKeyOptions, fingerprints);
  } else {
    return Promise.reject(new Errors.KeyManagerError('No scheme found by that name.'));
  }
}

KeyManager.prototype.handleError = function () {
  // TODO
};

/**
 * Gets a key system given a fingerprint.
 *
 * @param {string} fingerprint - key fingerprint to find.
 * @promise {KeySystem} - key system with given fingerprint.
 */
KeyManager.prototype.getKeySystem = function (fingerprint) {
  return KeyStorage.get(fingerprint);
};

/**
 * Gets all attributes from storage.
 *
 * @promise {KeyAttributes[]} - all key system attributes.
 */
KeyManager.prototype.getKeyAttributes = function () {
  return KeyStorage.getAllAttributes();
};

/**
 * Gets all selectable systems from key storage.
 *
 * @promise {KeySystem[]} - all selectable key systems.
 */
KeyManager.prototype.getSelectableSystems = function () {
  return KeyStorage.getAll().then(function (systems) {
    return systems.filter(function (keySystem) {
      return keySystem.attributes.canSelect;
    });
  });
};

KeyManager.prototype.encryptDraft = function (message) {
  return KeyStorage.getDraftEncryptionKey().then(function (key) {
    return Encryptor.encrypt(message, key);
  });
};

KeyManager.prototype.decryptDraft = function (encryptedMessage) {
  return KeyStorage.getDraftEncryptionKey().then(function (key) {
    return Encryptor.decrypt(encryptedMessage, key);
  });
};

KeyManager.prototype.addEventListener = function () {
  // TODO
};

KeyManager.prototype.ensureKeyStoragePasswordPresent = function () {
  return KeyStorage.ensureMasterPasswordPresent();
};

KeyManager.prototype.checkStorageMasterPassword = function (password) {
  return KeyStorage.checkMasterPassword(password);
};

KeyManager.prototype.setStorageMasterPassword = function (password) {
  return KeyStorage.setMasterPassword(password);
};

KeyManager.prototype.promptMasterPassword = function (originalError) {
  var self = this;

  var modal           = $('#master-password-modal'),
      modalHeader     = modal.find('.master-password-header').text(''),
      promptField     = modal.find('.master-password-prompt').text(''),
      passwordField   = modal.find('.master-password').off().val(''),
      passwordConfirm = modal.find('.master-password-confirm').off().val(''),
      submitButton    = modal.find('.create-btn').off().prop('disabled', false),
      errorContainer  = modal.find('.alert').hide(),
      resetCheckbox   = modal.find('.alert .alert-text #master-password-reset').off().prop('checked', false);

  var passwordFields = passwordField.add(passwordConfirm);

  var headerText, promptText;

  if (originalError instanceof Errors.MasterPasswordMissing) {
    headerText = 'Set up your master password';
    promptText =
        'You must set up a master password in order to protect your encryption keys. ' +
        'You will need to re-enter this password whenever you restart your browser.' +
        '<br><br><span style="color:red">If you have created a MessageGuard.io account, ' +
        'this master password <i>should be different from that account\'s password.</i></span>';
  } else if (originalError instanceof Errors.MasterPasswordNeedsReentering) {
    headerText = 'Re-enter your master password';
    promptText = 'You need to re-enter your master password in order to unlock MessageGuard.';
  } else {
    headerText = 'Enter your master password to decrypt your keys';
    promptText = 'You must enter a master password to unlock MessageGuard.';
  }

  modalHeader.html(headerText);
  promptField.html(promptText);

  return new Promise(function (resolve) {
    modal.modal('show');

    passwordFields.keypress(function (e) {
      if (e.which == 13 && !submitButton.is(':disabled')) {
        submitButton.click();
      }
    });

    passwordFields.on('input', function () {
      var isSaveable = passwordField.val() == passwordConfirm.val() && passwordField.val() != '';
      submitButton.prop('disabled', !isSaveable);
    }).trigger('input');

    submitButton.click(function () {
      var password = passwordField.val();
      errorContainer.hide();
      submitButton.prop('disabled', true);
      self.checkStorageMasterPassword(password).then(function (isValid) {
        if (isValid) {
          return resolve(password);
        }

        if (resetCheckbox.is(':checked')) {
          KeyStorage.clearAll().then(function () {
            resolve(password);
          });
        }

        errorContainer.show();
        submitButton.prop('disabled', false);
      });
    });
  }).then(function (password) {
    return self.setStorageMasterPassword(password);
  }).then(function () {
    modal.modal('hide');
  });
};

module.exports = new KeyManager();